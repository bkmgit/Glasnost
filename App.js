import React from 'react';
import {Provider} from 'react-redux';
import {SafeAreaView, View} from 'react-native';

import {Router} from "router/Router";
import {getStore} from 'store';
import {mainPageStyle, Loader, Popup, colors} from 'elements';
import {setStore} from 'api/utils';

const store = getStore();
setStore(store);

export * from './src/routes';

function App() {
    return (
        <Provider store={store}>
            <SafeAreaView style={{flex: 1, backgroundColor: colors.backgroundColor}}>
                <View style={mainPageStyle}>
                    <Router/>
                    <Loader/>
                    <Popup/>
                </View>
            </SafeAreaView>
        </Provider>
    );
}

export default App;
