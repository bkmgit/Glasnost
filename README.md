# [Glasnost](https://puskin.it/glasnost/) 

![gplV3][shield-gplv3]
![download from google play][shield-gplay]

### A native mobile client for Gitlab

![dark theme][img-dark]
![light theme][img-light]

### Features

- support for issues, merge request, commits, and file explorer
- support for multiple Gitlab hosts (so you can work both on your company’s Gitlab and on Gitlab.com at the same time)
- two different themes (a light one and a dark one)
- a lite version for when your data connection is stuck on edge
- support for fingerprint authentication
- ...and much more!

### Issues/Bugs/Suggestions

Feel free to report bugs or suggest improvements [here][issues].

### Contributing

Would you like to help developing Glasnost? Take a look to [our guide][contributing]!

### Android version

[<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' width="200"/>][download-gplay]

### License

Glasnost is licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) licensed.

[shield-gplv3]: https://img.shields.io/badge/license-GPLv3-blue
[shield-gplay]: https://img.shields.io/badge/download-Google_Play-brightgreen?logo=googleplay
[download-gplay]: https://play.google.com/store/apps/details?id=com.puskin.glasnost
[contributing]: CONTRIBUTING.md
[issues]: https://gitlab.com/puskin/Glasnost/issues
[img-dark]: screenshots/overview-dark.jpg
[img-light]: screenshots/overview-light.jpg
