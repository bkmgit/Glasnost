module.exports = {
    preset: 'react-native',
    testEnvironment: 'jsdom',
    collectCoverage: true,
    setupFilesAfterEnv: ['<rootDir>/setupTest.js'],
    transform: {
        '^.+\\.jsx?$': 'babel-jest'
    },

    transformIgnorePatterns: [
        'node_modules/(?!react-native)/'
    ]
};
