'use strict'
var refractorMarkupTemplating = require('./markup-templating.js')
module.exports = soy
soy.displayName = 'soy'
soy.aliases = []
function soy(Prism) {
  Prism.register(refractorMarkupTemplating)
  ;(function(Prism) {
    Prism.languages.soy = {
    } // Tokenize all inline Soy expressions
    Prism.hooks.add('before-tokenize', function(env) {
      Prism.languages['markup-templating'].buildPlaceholders(
        env,
        'soy',
        '',
        function(match) {
          return false
        }
      )
    }) // Re-insert the tokens after tokenizing
    Prism.hooks.add('after-tokenize', function(env) {
      Prism.languages['markup-templating'].tokenizePlaceholders(env, 'soy')
    })
  })(Prism)
}
