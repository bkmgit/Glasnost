import React, {Component} from 'react';
import {connect} from 'react-redux';
import {FlatList, View} from 'react-native';
import _ from 'lodash';

import {setFooter, setHeaderLeftButton} from 'headerFooter';
import {navigate, registerRoute} from 'router';
import {AppContainer, colors, EmptyData, FormattedText, Icon, ItemList} from 'elements';

import {requestBranches, setBranch} from './actions';
import {getDefaultBranchName, getBranches} from './selectors';
import {Footer} from '../repository/Footer';


class BranchesComponent extends Component {
    componentDidMount() {
        const {requestBranches, setFooter, setHeaderLeftButton} = this.props;

        requestBranches();
        setHeaderLeftButton(this.headerLeftButton());
        setFooter(this.footer());
    }

    headerLeftButton() {
        return <Icon name={'angle-left'} size={25} style={{padding: 5}} onPress={() => this.props.navigate('Repository')}/>;
    }

    footer() {
        return <Footer/>;
    }

    onScroll({nativeEvent}) {
        const {setFooter} = this.props;
        const currentOffset = nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);

        if (dif < 0) {
            setFooter(this.footer());
        } else {
            setFooter();
        }

        this.offset = currentOffset;
    }

    render() {
        const {branches, currentBranch, setBranch, navigate} = this.props;

        return (
            <AppContainer>
                {!_.size(branches) && <EmptyData/>}
                <FlatList
                    data={branches}
                    keyExtractor={({name}) => name}
                    onScroll={e => this.onScroll(e)}
                    renderItem={({item}) => <SingleBranch
                        branch={item}
                        isSelected={currentBranch === item.name}
                        setBranch={setBranch}
                        navigate={navigate}
                    />}/>
            </AppContainer>
        );
    }
}

function SingleBranch({branch, isSelected, setBranch, navigate}) {
    return (
        <ItemList onPress={() => {
            setBranch(branch.name);
            navigate('Repository');
        }}>
            <View style={{flex: 1}}>
                <FormattedText>{branch.name}</FormattedText>
            </View>
            {isSelected && <Icon name={'mobile-issue-close'} size={20} color={colors.success}/>}
        </ItemList>
    );
}

const Branches = connect(state => ({
    branches: getBranches(state),
    currentBranch: getDefaultBranchName(state)
}), {
    requestBranches, setBranch, navigate, setFooter, setHeaderLeftButton
})(BranchesComponent);
registerRoute({Branches});
