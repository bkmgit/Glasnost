import _ from 'lodash';

import {get} from 'api';
import {getRepository} from '../repository/selectors';
import {requestPipelinesForBranch} from '../pipelines/actions';

export function requestBranches() {
    return async (dispatch, getState) => {
        const state = getState();
        const repo = getRepository(state);

        if (_.isEmpty(repo)) {
            return;
        }

        const {data} = await get({
            path: `projects/${repo.id}/repository/branches`,
            additionalParams: {per_page: 100}
        });

        if (data) {
            dispatch({type: 'BRANCHES_RECEIVED', data});
        }
    };
}

export function setBranch(branch) {
    return async (dispatch, getState) => {
        const state = getState();
        const projectId = getRepository(state).id;

        dispatch(await requestPipelinesForBranch(projectId, branch));
        dispatch({type: 'CURRENT_BRANCH', branch});
    };
}
