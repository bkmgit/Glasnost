import _ from 'lodash';
import {createSelector} from 'reselect';

const getBranchesSelector = state => _.get(state, 'branches');
export const getBranches = createSelector(getBranchesSelector, branches => _.get(branches, 'branches'));
export const getDefaultBranch = createSelector(getBranchesSelector, branches => _.find(branches.branches, {default: true}));
export const getDefaultBranchName = createSelector(getDefaultBranch, branch => _.get(branch, 'name'));
