import {getRegisteredRoutes} from 'router';
import {getReducers} from 'store';

import * as index from '../index';

describe('Commits index', () => {
    it('should export Commits and reducers', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().Commits).toBeDefined();
        expect(getRegisteredRoutes().CommitDiff).toBeDefined();
        expect(getReducers().commits).toBeDefined();
    });
});
