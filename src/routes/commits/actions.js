import _ from 'lodash';

import {get} from 'api';
import {getPagination, setPagination} from 'utils';

import {getRepository} from '../repository/selectors';

export function clearCommits() {
    return {type: 'CLEAR_COMMITS'};
}

export function requestCommits(page0) {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        if (_.isEmpty(repo)) {
            return;
        }

        const {pageToLoad = 0} = getPagination(getState(), 'commits');

        const {data, headers} = await get({
            page: page0 ? 0 : pageToLoad,
            path: `projects/${repo.id}/repository/commits`,
            cached: !page0
        });

        if (data) {
            dispatch({type: 'COMMITS_RECEIVED', data});
            dispatch(setPagination('commits', headers));
        }
    };
}

export function requestCommitDiffs(commit) {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        const {data} = await get({
            path: `projects/${repo.id}/repository/commits/${commit.id}/diff`,
            additionalParams: {per_page: 100},
            pagination: false
        });

        if (data) {
            dispatch({type: 'COMMIT_DIFFS_RECEIVED', data});
        }
    };
}
