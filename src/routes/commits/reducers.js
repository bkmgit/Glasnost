import {registerReducer} from 'store';
import _ from 'lodash';

function commits() {
    return {
        COMMITS_RECEIVED: (state, {data}) => ({...state, commits: _.uniqBy([...(state.commits || []), ...data], 'id')}),
        SET_COMMITS_PAGINATION: (state, {pagination}) => ({...state, pagination}),
        COMMIT_DIFFS_RECEIVED: (state, {data}) => ({...state, diffs: data}),
        CLEAR_COMMITS: state => ({...state, commits: []})
    };
}

registerReducer('commits', commits());
