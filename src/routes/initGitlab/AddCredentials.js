import {View, Linking} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';

import {navigate, registerRoute} from 'router';
import {
    Card,
    FormattedText,
    Icon,
    MainButton,
    ScrollableAppContainer,
    CircledImage, SimpleInput
} from 'elements';
import {
    showMessage,
    setNewUrl,
    getNewGitlabUrl,
    cleanUrl
} from 'utils';
import {is200} from 'api';
import {setTitle} from 'headerFooter';

import {validLogin} from './actions';

export const redirectUrl = 'glasnost://com.puskin.glasnost/android/callback';

class AddCredentialsComponent extends Component {
    state = {accessToken: '', access_token: '', isHelperVisible: false};

    componentDidMount() {
        this.props.setTitle('Personal Access Token');
    }

    componentWillUnmount() {
        this.props.setNewUrl('');
    }

    async addCredentials() {
        const {accessToken} = this.state;
        const authType = 'token';
        const {validLogin} = this.props;
        const urlWithProtocol = cleanUrl(this.props.url);
        const validToken = await is200(urlWithProtocol, accessToken, authType);

        if (validToken) {
            validLogin({validUrl: urlWithProtocol, accessToken, authType});
        } else {
            showMessage({error: true, message: 'Invalid Token!'});
        }
    }

    render() {
        const {accessToken, isHelperVisible} = this.state;
        const {navigate, url} = this.props;

        return (
            <ScrollableAppContainer>
                <View style={{margin: 70, flexDirection: 'column', alignItems: 'center'}}>
                    <View style={{justifyContent: 'center', alignItems: 'center', marginBottom: 50}}>
                        <CircledImage local={true} source={require('../../../assets/logo.png')} size={120}/>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Icon name={'information'} size={25} onPress={() => this.setState({isHelperVisible: !isHelperVisible})}/>
                        <SimpleInput
                            secureTextEntry={true}
                            placeholder={'Personal Access Token'}
                            style={{marginLeft: 10}}
                            autoCapitalize={'none'}
                            onChangeText={accessToken => this.setState({accessToken})}
                            value={accessToken}
                        />
                    </View>
                </View>

                {isHelperVisible
                && <Card title={'What is a Personal Access Token'} icon={{name: 'key'}}>
                    <FormattedText style={{fontSize: 16}}>
                        Personal Access Tokens are the most secure way you can give the application access to Gitlab
                        APIs.
                    </FormattedText>
                    <FormattedText style={{fontSize: 16}}>
                        Typing every time username and password can be risky and boring:
                        if any of the information gets lost or stolen, you are forced to change the password of the
                        account.
                    </FormattedText>
                    <FormattedText style={{fontSize: 16}}>
                        With Personal Access Tokens, Gitlab gives you a specific key with a limited scope and with a
                        possible expiration date.
                    </FormattedText>
                    <FormattedText style={{fontSize: 16, marginBottom: 10}}>
                        In order to get your key, just click the button below and give the application the "api" scope.
                    </FormattedText>

                    <MainButton
                        text={'Generate Personal Access Token'}
                        onPress={() => Linking.openURL(`${cleanUrl(url)}/profile/personal_access_tokens`)}/>
                </Card>
                }

                <View style={{justifyContent: 'space-around', flexDirection: 'row'}}>
                    <MainButton secondary={true} onPress={() => navigate('AddGitlabUrl')} text={'Change Url'}/>
                    <MainButton onPress={() => this.addCredentials()} text={'Add Credentials'} disabled={!accessToken}/>
                </View>
            </ScrollableAppContainer>
        );
    }
}

const AddCredentials = connect(state => ({url: getNewGitlabUrl(state)}), {
    setNewUrl,
    validLogin,
    navigate,
    setTitle
})(AddCredentialsComponent);
registerRoute({AddCredentials});
