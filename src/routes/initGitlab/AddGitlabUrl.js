import {Linking, View} from 'react-native';
import React, {Component} from 'react';
import _ from 'lodash';
import {setTitle} from 'headerFooter';
import {connect} from 'react-redux';

import {navigate, registerRoute} from 'router';
import {cleanUrl, setNewUrl, showMessage} from 'utils';
import {MainButton, CircledImage, ScrollableAppContainer, SimpleInput} from 'elements';

import {is200} from 'api';
import {redirectUrl} from './AddCredentials';
import {validLogin} from './actions';

function isUrlValid(url) {
    return url.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
}

class AddGitlabUrlComponent extends Component {
    state = {gitlabUrl: 'https://gitlab.com', urlIsValid: true};

    /* istanbul ignore next */
    componentDidMount() {
        this.props.setTitle('Add Gitlab instance');
        Linking.getInitialURL().then(url => this.handleNavigation(url));
        Linking.addEventListener('url', url => this.handleNavigation(url));
    }

    /* istanbul ignore next */
    handleNavigation(url) {
        const extractedUrl = _.get(url, 'url', url);

        if (_.includes(extractedUrl, redirectUrl)) {
            const accessTokenRex = new RegExp(`${redirectUrl}#access_token=`, 'gi');
            const tokenTypeRex = new RegExp('&token_type=bearer', 'gi');

            const access_token = extractedUrl.replace(accessTokenRex, '').replace(tokenTypeRex, '');

            if (access_token) {
                Linking.removeEventListener('url', url => this.handleNavigation(url));
                this.addCredentials(access_token);
            }
        }
    }

    /* istanbul ignore next */
    async addCredentials(token) {
        const authType = 'bearer';
        const {validLogin} = this.props;
        const urlWithProtocol = cleanUrl(this.state.gitlabUrl);
        const validToken = await is200(urlWithProtocol, token, authType);

        if (validToken) {
            validLogin({validUrl: urlWithProtocol, accessToken: token, authType});
        } else {
            showMessage({error: true, message: 'Invalid Token!'});
        }
    }

    async oauthAction() {
        const clientID = 'a1eacab8fc240959afd4bb915f3b220d14417d43fba85855cc8764d2d8c65456';
        const domain = `https://gitlab.com/oauth/authorize?client_id=${clientID}`
            + `&redirect_uri=${encodeURIComponent(redirectUrl)}&response_type=token`;

        Linking.openURL(domain);
    }

    checkUrl(gitlabUrl) {
        this.setState({gitlabUrl, urlIsValid: isUrlValid(gitlabUrl)});
    }

    isGitlabDotCom() {
        const url = cleanUrl(this.state.gitlabUrl);

        return url.indexOf('//gitlab.com') !== -1;
    }

    render() {
        const {gitlabUrl, urlIsValid} = this.state;
        const {setNewUrl, navigate} = this.props;
        const isGitlab = this.isGitlabDotCom();

        return (
            <ScrollableAppContainer>
                <View style={{margin: 70}}>
                    <View style={{justifyContent: 'center', alignItems: 'center', marginBottom: 50}}>
                        <CircledImage local={true} source={require('../../../assets/logo.png')} size={120}/>
                    </View>
                    <SimpleInput
                        autoCapitalize={'none'}
                        autoCompleteType={'off'}
                        autoCorrect={false}
                        onChangeText={gitlabUrl => this.checkUrl(gitlabUrl)}
                        value={gitlabUrl}
                    />
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                    <MainButton
                        onPress={() => {
                            navigate('AddCredentials');
                            setNewUrl(gitlabUrl);
                        }}
                        secondary={isGitlab}
                        text={'Access Token'}
                        disabled={!urlIsValid}
                    />
                    {isGitlab && <MainButton onPress={() => this.oauthAction()} text={'OAuth Login'}/>}
                </View>
            </ScrollableAppContainer>
        );
    }
}

const AddGitlabUrl = connect(null, {setNewUrl, validLogin, navigate, setTitle})(AddGitlabUrlComponent);
registerRoute({AddGitlabUrl});
