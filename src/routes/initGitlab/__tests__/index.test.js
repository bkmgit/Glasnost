import {getRegisteredRoutes} from 'router';

import * as index from '../index';

describe('initGitlab index', () => {
    it('should export initGitlab components', () => {
        expect(index.redirectUrl).toEqual('glasnost://com.puskin.glasnost/android/callback');
        expect(getRegisteredRoutes().AddCredentials).toBeDefined();
        expect(getRegisteredRoutes().AddGitlabUrl).toBeDefined();
        expect(getRegisteredRoutes().HardwareLogin).toBeDefined();
        expect(getRegisteredRoutes().ScanFingerprint).toBeDefined();
    });
});
