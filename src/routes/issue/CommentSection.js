import React, {Component} from 'react';
import {TextInput, View} from 'react-native';
import {connect} from 'react-redux';

import {MainButton, mainInputStyle} from 'elements';

import {commentIssue} from './actions';

class CommentSectionComponent extends Component {
    state = {body: ''};

    render() {
        const {commentIssue, onClose} = this.props;
        const {body} = this.state;

        return (
            <View style={{flexDirection: 'column'}}>
                <TextInput
                    autoFocus={true}
                    multiline={true}
                    onChangeText={body => this.setState({body})}
                    style={{...mainInputStyle, height: 80}}
                    placeholder={'Write a comment...'}
                />
                <View style={{flexDirection: 'row', marginVertical: 20, justifyContent: 'space-around'}}>
                    <MainButton secondary={true} text={'Close'} onPress={onClose}/>
                    <MainButton text={'Comment'} onPress={() => {
                        commentIssue({body});
                        onClose();
                    }}/>
                </View>
            </View>
        );
    }
}

export const CommentSection = connect(null, {commentIssue})(CommentSectionComponent);
