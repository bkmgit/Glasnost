import React from 'react';
import {View} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';

import {navigate} from 'router';
import {CircledImage} from 'elements';


function AvatarsComponent({assignee, author, navigate}) {
    if (!assignee || !author) {
        return null;
    }

    const assigneeImage = _.get(assignee, 'avatar_url');
    const assigneeName = _.get(assignee, 'name');
    const assigneeId = _.get(assignee, 'id');
    const authorImage = _.get(author, 'avatar_url');
    const authorName = _.get(author, 'name');
    const authorId = _.get(author, 'id');
    const authorDifferentFromAssignee = author.id !== assignee.id;

    return (
        <View style={{
            alignItems: 'center', flex: 0.2, flexDirection: 'column', marginRight: 10
        }}>
            <CircledImage
                onPress={() => navigate('Profile', {id: authorId})}
                url={authorImage}
                name={authorName}
                size={authorDifferentFromAssignee ? 30 : 50}/>
            {authorDifferentFromAssignee
            && <CircledImage url={assigneeImage} name={assigneeName} size={30}
                onPress={() => navigate('Profile', {id: assigneeId})}/>}
        </View>
    );
}

export const Avatars = connect(null, {navigate})(AvatarsComponent);
