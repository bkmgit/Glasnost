import {getRegisteredRoutes} from 'router';
import {getReducers} from 'store';

import * as index from '../index';

describe('Issues index', () => {
    it('should export Issues and reducers', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().Issues).toBeDefined();
        expect(getReducers().issues).toBeDefined();
    });
});
