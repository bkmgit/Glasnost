import React, {Component} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {CodeDisplayer, EmptyData, FormattedText, Icon, ScrollableAppContainer, colors} from 'elements';
import {getDiffs} from './selectors';
import {requestMrDiffs} from './actions';
import {requestCommitDiffs} from '../commits/actions';
import {getCommitDiffs} from '../commits/selectors';

function FileName({diff}) {
    const {renamed_file, old_path, new_path} = diff;

    if (renamed_file) {
        return (
            <View style={{
                flex: 1, flexWrap: 'wrap', flexDirection: 'row', paddingHorizontal: 10
            }}>
                <FormattedText style={{textDecorationLine: 'line-through'}}>{old_path}</FormattedText>
                <FormattedText>{' => '}</FormattedText>
                <FormattedText>{new_path}</FormattedText>
            </View>
        );
    }

    return (
        <View style={{flex: 1, flexWrap: 'wrap', paddingHorizontal: 15}}>
            <FormattedText>{old_path}</FormattedText>
        </View>
    );
}

function getFlagsAndCode(code) {
    const splitted = code.split('\n');
    const cleanCode = [];
    const flags = [];

    for (let line of splitted) {
        const firstChar = _.first(line);
        const flag = {height: 17, width: 5};

        if (firstChar === '@' && line[1] === '@') {
            continue;
        } else if (firstChar === '+') {
            flag.backgroundColor = colors.success;
            line = line.substr(1);
        } else if (firstChar === '-') {
            flag.backgroundColor = colors.warning;
            line = line.substr(1);
        } else {
            flag.backgroundColor = 'transparent';
        }

        flags.push(<View key={_.size(flags)} style={flag}/>);
        cleanCode.push(line);
    }

    return {flags, code: cleanCode.join('\n')};
}

function DiffFile({diff}) {
    const {flags, code} = getFlagsAndCode(diff.diff || diff);

    return (
        <ScrollableAppContainer style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flexDirection: 'column', marginVertical: 8}}>{flags}</View>
            <CodeDisplayer>{code}</CodeDisplayer>
        </ScrollableAppContainer>
    );
}

class DiffComponent extends Component {
    state = {visibleDiffs: []};

    componentDidMount() {
        const {commit, requestMrDiffs, requestCommitDiffs} = this.props;

        if (commit) {
            requestCommitDiffs(commit);
        } else {
            requestMrDiffs();
        }
    }

    handleVisibility(index) {
        const {visibleDiffs} = this.state;

        if (_.includes(visibleDiffs, index)) {
            this.setState({visibleDiffs: _.filter(visibleDiffs, visibleIndex => visibleIndex !== index)});
        } else {
            this.setState({visibleDiffs: [...visibleDiffs, index]});
        }
    }

    render() {
        const {diffs, commit} = this.props;
        const {visibleDiffs} = this.state;

        if (!diffs) {
            return <EmptyData/>;
        }

        const codeDiff = commit ? diffs : _.get(diffs, 'diffs', []);

        return _.map(codeDiff, (diff, index) => (
            <View key={index} style={{marginVertical: 10}}>
                <TouchableOpacity onPress={() => this.handleVisibility(index)} style={{flexDirection: 'row'}}>
                    <FileName diff={diff}/>
                    <Icon name={_.includes(visibleDiffs, index) ? 'angle-up' : 'angle-down'} size={20}/>
                </TouchableOpacity>
                {_.includes(visibleDiffs, index) && <DiffFile diff={diff}/>}
            </View>
        ));
    }
}

export class Diffs extends Component {
    constructor(props) {
        super(props);

        this.state = {areDiffsVisible: !!props.commit};
    }

    render() {
        const {commit} = this.props;
        const {areDiffsVisible} = this.state;

        return (
            <View>
                {!commit && <TouchableOpacity
                    style={{flexDirection: 'row', alignItems: 'center', marginVertical: 10}}
                    onPress={() => this.setState({areDiffsVisible: !areDiffsVisible})}
                >
                    <Icon name={'expand'} size={25}/>
                    <FormattedText style={{fontSize: 18, marginLeft: 10}}>{'Toggle diffs'}</FormattedText>
                </TouchableOpacity>}
                {areDiffsVisible && <Diff commit={commit}/>}
            </View>
        );
    }
}

const Diff = connect((state, {commit}) => ({diffs: commit ? getCommitDiffs(state) : getDiffs(state)}), {requestMrDiffs, requestCommitDiffs})(DiffComponent);
