import React, {Component} from 'react';
import {Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {
    FormattedText,
    colors,
    Markdown,
    ScrollableAppContainer,
    AppContainer
} from 'elements';
import {getProjectPermissionLevel} from 'utils';
import {setFooter, setHeaderRightButton} from 'headerFooter';
import {registerRoute} from 'router';
import {getMergeRequest} from './selectors';
import {requestMergeRequest, editMergeRequest} from './actions';
import {Discussions} from './Discussions';
import {Jobs} from '../pipelines';
import {Diffs} from './Diffs';
import {CommentSection} from './CommentSection';
import {getMe} from '../profile/selectors';
import {getRepository} from '../repository/selectors';
import {Stats} from './Stats';
import {Assignees} from '../issue/Assignees';
import {Header} from './Header';

class MergeRequestComponent extends Component {
    state = {isCommentSectionVisibile: false};

    componentDidMount() {
        const {requestMergeRequest, iid, projectId} = this.props;

        requestMergeRequest(iid, projectId);
    }

    headerRightButton() {
        const {mergeRequest, repository, editMergeRequest, me} = this.props;
        const {state, author} = mergeRequest;
        const permissions = _.get(repository, 'permissions');
        const project_access = _.get(permissions, 'project_access', {});
        const group_access = _.get(permissions, 'group_access', {});
        const authorId = _.get(author, 'id');
        const hasPermission = (authorId === _.get(me, 'id') || getProjectPermissionLevel(project_access, group_access) >= 20);
        const canClose = state === 'opened' && hasPermission;
        const canReopen = state === 'closed' && hasPermission;

        if (canClose) {
            return (
                <TouchableHighlight onPress={() => editMergeRequest({state_event: 'close'})}>
                    <Text style={{color: colors.warning}}>{'Close'}</Text>
                </TouchableHighlight>
            );
        }
        if (canReopen) {
            return (
                <TouchableHighlight onPress={() => editMergeRequest({state_event: 'reopen'})}>
                    <FormattedText>{'Reopen'}</FormattedText>
                </TouchableHighlight>
            );
        }

        return null;
    }

    footer() {
        const {mergeRequest: {state}} = this.props;
        const {isCommentSectionVisibile} = this.state;

        if (state !== 'opened' || isCommentSectionVisibile) {
            return null;
        }

        return (
            <TouchableOpacity style={{flex: 1}} onPress={() => this.setState({isCommentSectionVisibile: true})}>
                <FormattedText style={{fontSize: 20, textDecorationLine: 'underline', textAlign: 'center'}}>{'Write a comment'}</FormattedText>
            </TouchableOpacity>
        );
    }

    render() {
        const {mergeRequest, iid, projectId, setHeaderRightButton, setFooter} = this.props;
        const {isCommentSectionVisibile} = this.state;

        if (!mergeRequest) {
            return <AppContainer/>;
        }

        setHeaderRightButton(this.headerRightButton());
        setFooter(this.footer());

        return (
            <ScrollableAppContainer refresh={() => requestMergeRequest(iid, projectId, true)} style={{marginTop: 20}}>
                <Header/>
                <View style={{alignItems: 'center', margin: 10}}>
                    <Assignees assignees={_.get(mergeRequest, 'assignee')}/>
                </View>
                <Stats/>
                <Description mergeRequest={mergeRequest}/>
                <Jobs pipelineId={_.get(mergeRequest, 'pipeline.id')}/>
                <Diffs/>
                {isCommentSectionVisibile && <CommentSection onClose={() => this.setState({isCommentSectionVisibile: false})}/>}
                <Discussions/>
            </ScrollableAppContainer>
        );
    }
}

function Description({mergeRequest}) {
    const {description, web_url = ''} = mergeRequest;
    const baseUrl = (web_url.match(/(.+)\/merge_requests\/\d+/) || [])[1];

    if (!description) {
        return null;
    }

    return <Markdown text={description} baseUrl={baseUrl}/>;
}


const MergeRequest = connect(state => ({
    mergeRequest: getMergeRequest(state),
    repository: getRepository(state),
    me: getMe(state)
}), {
    requestMergeRequest,
    editMergeRequest,
    setHeaderRightButton,
    setFooter
})(MergeRequestComponent);
registerRoute({MergeRequest});
