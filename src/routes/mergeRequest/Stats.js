import React from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {Card, colors, FormattedText, Icon} from 'elements';
import {getBasicDateFormat} from 'utils';

import {Thumbs} from './Thumbs';
import {getMergeRequest} from './selectors';

export function Stats() {
    return (
        <Card title={'Stats'} icon={{name: 'overview'}}>
            <View style={{flexDirection: 'row', alignItems: 'center', marginHorizontal: 10, justifyContent: 'space-between'}}>
                <StatusInfos/>
                <Thumbs/>
            </View>
        </Card>
    );
}

function StatusIcon({iconName, color, text}) {
    return (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon name={iconName} size={30} color={color}/>
            <FormattedText style={{marginLeft: 5, fontWeight: 'bold'}}>{text}</FormattedText>
        </View>
    );
}

function Milestone({milestone}) {
    if (_.isEmpty(milestone)) {
        return null;
    }

    const {title, due_date} = milestone;

    return (
        <View>
            <FormattedText>{`Milestone: ${title}`}</FormattedText>
            {due_date && <FormattedText>{`Due Date: ${getBasicDateFormat(due_date)}`}</FormattedText>}
        </View>
    );
}

function StatusInfosComponent({mergeRequest}) {
    const {merge_status, milestone} = mergeRequest;
    return (
        <View>
            {merge_status === 'cannot_be_merged'
            && <StatusIcon iconName={'warning'} color={colors.warning} text={'Cannot be merged!'}/>}
            {merge_status === 'can_be_merged'
            && <StatusIcon iconName={'status_success_borderless'} color={colors.success} text={'Can be merged'}/>}
            {merge_status === 'unchecked'
            && <StatusIcon iconName={'status_notfound_borderless'} color={colors.lighGray} text={'Unchecked'}/>}
            <Milestone milestone={milestone}/>
        </View>
    );
}

const StatusInfos = connect(state => ({mergeRequest: getMergeRequest(state)}))(StatusInfosComponent);
