import React from 'react';
import _ from 'lodash';
import {TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';

import {FormattedText, Icon} from 'elements';

import {getThumbs} from './selectors';
import {getMe} from '../profile/selectors';
import {addThumbs, removeThumbs} from './actions';

function ThumbsComponent({thumbs, addThumbs, removeThumbs, me}) {
    const upvotes = _.filter(thumbs, {name: 'thumbsup'});
    const downvotes = _.filter(thumbs, {name: 'thumbsdown'});
    const hasUpvoted = _.find(upvotes, upvote => upvote.user.id === me.id);
    const hasDownvoted = _.find(downvotes, downvote => downvote.user.id === me.id);

    return (
        <View style={{flexDirection: 'row', justifyContent: 'flex-end', flex: 0.5}}>
            <TouchableOpacity
                style={{flexDirection: 'row', marginRight: 10, alignItems: 'center'}}
                onPress={() => (hasUpvoted ? removeThumbs(hasUpvoted.id) : addThumbs('thumbsup'))}
            >

                <Icon name={'thumb-up'} size={20}/>
                <FormattedText style={{fontSize: 20, marginLeft: 5}}>{upvotes.length || 0}</FormattedText>

            </TouchableOpacity>
            <TouchableOpacity
                style={{flexDirection: 'row', alignItems: 'center'}}
                onPress={() => (hasDownvoted ? removeThumbs(hasDownvoted.id) : addThumbs('thumbsdown'))}
            >

                <Icon name={'thumb-down'} size={20}/>
                <FormattedText style={{fontSize: 20, marginLeft: 5}}>{downvotes.length || 0}</FormattedText>

            </TouchableOpacity>
        </View>
    );
}

export const Thumbs = connect(state => ({thumbs: getThumbs(state), me: getMe(state)}), {addThumbs, removeThumbs})(ThumbsComponent);
