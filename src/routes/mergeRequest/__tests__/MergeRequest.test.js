import React from 'react';
import {Provider} from 'react-redux';
import {getRegisteredRoutes} from 'router';
import {getFooter, getHeaderRightButton} from 'headerFooter/selectors';

const fakeMergeRequest = {
    id: 1,
    title: 'someTitle',
    state: 'opened',
    description: 'someDescription',
    merge_status: 'can_be_merged',
    web_url: 'someUrl',
    created_at: '2019-05-11T14:30:23.406Z',
    updated_at: '2019-05-11T14:30:23.406Z',
    author: {id: 1},
    milestone: {title: 'someTitle', due_date: '2019-05-11T14:30:23.406Z'},
    assignees: [],
    labels: ['some', 'label']
};

const fakeDiscussions = [
    {id: 1, individual_note: false, notes: [{type: 'DiffNote', resolved: false, author: {username: 'someUsername'}, body: 'someBody'}]},
    {id: 2, individual_note: false, notes: [{type: 'DiscussionNote', resolved: true, author: {username: 'someUsername2'}, body: 'someBody2'}]},
    {id: 3, individual_note: false, notes: [{type: 'DiffNote', resolved: true, author: {username: 'someUsername3', id: 2}, body: 'someBody3'}]},
    {id: 4, individual_note: false, notes: [{type: 'ops', resolved: true, author: {username: 'someUsername3'}, body: 'someBody3'}]},
    {id: 5, individual_note: true, notes: [{type: 'ops', resolved: true, author: {username: 'someUsername3'}, body: 'someBody3'}]}
];

const fakeThumbs = [
    {name: 'thumbsup', user: {id: 1}},
    {name: 'thumbsup', user: {id: 2}},
    {name: 'thumbsdown', user: {id: 1}},
    {name: 'thumbsdown', user: {id: 2}}
];


const fakeDiffs = {
    diffs: [
        {
            renamed_file: 'renamedFile',
            new_path: 'newPath',
            diff: `
            diff --git a/setupTest.js b/setupTest.js
index 4b05446..a706a26 100644
--- a/setupTest.js
+++ b/setupTest.js
@@ -33,3 +33,17 @@ beforeEach(() => {
     require('./App');
 });
 
+function suppressDomErrors() {
+    const suppressedErrors = /(attribute \`accessible\`|The tag.*is unrecognized in this browser|PascalCase)/;
+    // eslint-disable-next-line no-console
+    const realConsoleError = console.error;
+    // eslint-disable-next-line no-console
+    console.error = message => {
+        if (message.match(suppressedErrors)) {
+            return;
+        }
+        realConsoleError(message);
+    };
+}
+
+suppressDomErrors();
\\ No newline at end of file

            `
        },
        {
            old_path: 'oldPath'
        }
    ]
};

describe('MergeRequest', () => {
    let MergeRequest;
    let store;
    let actions;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        actions = require('../actions');
        require('../index');
        require('../../repository/reducers');
        require('../../profile/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        MergeRequest = getRegisteredRoutes().MergeRequest;

        store = require('store').getStore();

        onGet({url: /discussions/, data: fakeDiscussions});
        onGet({url: /compare*/, data: fakeDiffs});
        onGet({url: /diff*/, data: fakeDiffs.diffs});
    });

    it('renders correctly the empty page', async () => {
        const component = await asyncCreate(<Provider store={store}><MergeRequest/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the mergeRequest', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: fakeMergeRequest});

        const component = await asyncCreate(<Provider store={store}><MergeRequest/></Provider>);
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders an open mergeRequest', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'opened'}});

        const component = await asyncCreate(<Provider store={store}><MergeRequest/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the mergeRequest with no milestone', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, milestone: {}, updated_at: '2019-05-13T14:30:23.406Z'}});

        const component = await asyncCreate(<Provider store={store}><MergeRequest/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the mergeRequest with no description', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, description: null}});

        const component = await asyncCreate(<Provider store={store}><MergeRequest/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the mergeRequest with some label', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, labels: ['some', 'label']}});

        const component = await asyncCreate(<Provider store={store}><MergeRequest/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('shows the Close MergeRequest button', async () => {
        store.dispatch({type: 'USER_RECEIVED', data: {id: 1}});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'opened', author: {id: 1}}});

        const component = mount(<Provider store={store}><MergeRequest/></Provider>);
        await waitForAsync();
        component.update();

        const CloseMergeRequest = getHeaderRightButton(store.getState());
        CloseMergeRequest.props.onPress();

        expect(component.html()).toMatchSnapshot();
    });

    it('shows the Reopen button', async () => {
        store.dispatch({type: 'USER_RECEIVED', data: {id: 1}});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'closed', author: {id: 1}}});

        const component = mount(<Provider store={store}><MergeRequest/></Provider>);
        await waitForAsync();
        component.update();

        const ReopenButton = getHeaderRightButton(store.getState());
        ReopenButton.props.onPress();

        expect(component.html()).toMatchSnapshot();
    });

    it('renders the mergeRequest with some assignee', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, assignees: [{name: 'someName1'}, {name: 'someName2'}]}});

        const component = await asyncCreate(<Provider store={store}><MergeRequest/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('opens the comment section', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'opened'}});

        const component = mount(<Provider store={store}><MergeRequest/></Provider>);
        await waitForAsync();
        component.update();

        const Footer = getFooter(store.getState());
        Footer.props.onPress();

        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should write in the comment section', () => {
        const {CommentSection} = require('../CommentSection');
        const component = mount(<Provider store={store}><CommentSection/></Provider>);

        const TextInput = component.find('TextInput').at(0);
        TextInput.props().onChangeText('someMessage2');
        TextInput.props().value = 'someMessage2';
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should comment the mergeRequest when clicking the Comment button', () => {
        const onClose = jest.fn();
        const {CommentSection} = require('../CommentSection');
        const component = mount(<Provider store={store}><CommentSection onClose={onClose}/></Provider>);

        const MainButton = component.find('MainButton').at(1);
        MainButton.props().onPress();
        component.update();

        expect(onClose).toHaveBeenCalled();
    });

    it('should get and display the discussions', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'opened', web_url: ''}});

        const component = mount(<Provider store={store}><MergeRequest/></Provider>);
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should refresh when scrolling down the list', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'opened', web_url: ''}});

        const component = mount(<Provider store={store}><MergeRequest/></Provider>);
        await waitForAsync();
        component.update();

        const ScrollableAppContainer = component.find('ScrollableAppContainerComponent').at(0);
        ScrollableAppContainer.props().refresh();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('clicks the thumbUp', async () => {
        store.dispatch({type: 'USER_RECEIVED', data: {id: 1}});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /award_emoji/, data: fakeThumbs});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'opened', web_url: ''}});

        const component = mount(<Provider store={store}><MergeRequest/></Provider>);
        await waitForAsync();
        component.update();

        const Thumbs = component.find('ThumbsComponent').find('TouchableOpacity').at(0);
        Thumbs.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('clicks the thumbDown', async () => {
        store.dispatch({type: 'USER_RECEIVED', data: {id: 1}});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /award_emoji/, data: fakeThumbs});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'opened', web_url: ''}});

        const component = mount(<Provider store={store}><MergeRequest/></Provider>);
        await waitForAsync();
        component.update();

        const Thumbs = component.find('ThumbsComponent').find('TouchableOpacity').at(1);
        Thumbs.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('shows the diffs', async () => {
        store.dispatch({type: 'USER_RECEIVED', data: {id: 1}});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
        onGet({url: /award_emoji/, data: fakeThumbs});
        onGet({url: /merge_requests*/, data: {...fakeMergeRequest, state: 'opened', web_url: ''}});

        const component = mount(<Provider store={store}><MergeRequest/></Provider>);
        await waitForAsync();
        component.update();

        const Diffs = component.find('Diffs').find('TouchableOpacity').at(0);
        Diffs.props().onPress();
        await waitForAsync();
        component.update();

        const Diff = component.find('DiffComponent').find('TouchableOpacity').at(0);
        Diff.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();

        Diff.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should add one thumb', async () => {
        let actions;
        jest.isolateModules(() => {
            actions = require('../actions');
            actions.requestMergeRequest = jest.fn().mockReturnValue({type: 'type'});
        });
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: {iid: 1}});
        store.dispatch(actions.addThumbs('direction'));
        await waitForAsync();

        expect(actions.requestMergeRequest).toHaveBeenCalled();
    });

    it('should not add one thumb', async () => {
        actions.requestMergeRequest = jest.fn().mockReturnValue({type: 'type'});
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: []});
        store.dispatch(actions.addThumbs('direction'));
        await waitForAsync();

        expect(actions.requestMergeRequest).toHaveBeenCalledTimes(0);
    });

    it('should remove one thumb', async () => {
        actions.requestMergeRequest = jest.fn().mockReturnValue({type: 'type'});
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: {iid: 1}});
        store.dispatch(actions.removeThumbs('id'));
        await waitForAsync();

        expect(actions.requestMergeRequest).toHaveBeenCalled();
    });

    it('should not remove one thumb', async () => {
        actions.requestMergeRequest = jest.fn().mockReturnValue({type: 'type'});
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: []});
        store.dispatch(actions.removeThumbs('id'));
        await waitForAsync();

        expect(actions.requestMergeRequest).toHaveBeenCalledTimes(0);
    });

    it('should not comment because there is no mr', async () => {
        actions.requestDiscussions = jest.fn().mockReturnValue({type: 'type'});
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: []});
        store.dispatch(actions.commentMergeRequest('body'));
        await waitForAsync();

        expect(actions.requestDiscussions).toHaveBeenCalled();
    });

    it('should not edit because there is no mr', async () => {
        actions.requestDiscussions = jest.fn().mockReturnValue({type: 'type'});
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: []});
        store.dispatch(actions.editMergeRequest('changes'));
        await waitForAsync();

        expect(actions.requestDiscussions).toHaveBeenCalledTimes(0);
    });

    it('should not requestDiscussions because there is no mr', async () => {
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: []});
        store.dispatch(actions.requestDiscussions('changes'));
        global.onGet = jest.fn();

        expect(onGet).toHaveBeenCalledTimes(0);
    });

    it('should not requestMrDiffs because there is no mr', async () => {
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: []});
        store.dispatch(actions.requestMrDiffs('changes'));
        global.onGet = jest.fn();

        expect(onGet).toHaveBeenCalledTimes(0);
    });

    it('should not requestThumbs because there is no mr', async () => {
        store.dispatch({type: 'MERGE_REQUEST_RECEIVED', data: []});
        store.dispatch(actions.requestThumbs('changes'));
        global.onGet = jest.fn();

        expect(onGet).toHaveBeenCalledTimes(0);
    });
});
