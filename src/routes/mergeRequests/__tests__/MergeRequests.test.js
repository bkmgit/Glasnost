import React from 'react';
import {Provider} from 'react-redux';
import {getRegisteredRoutes, getRouteName, navigate} from 'router';
import _ from 'lodash';
import moment from 'moment';
import {getPagination, setPagination} from 'utils';
import {getMergedRequests} from '../selectors';
import {clearMergeRequests, requestMergeRequests} from '../actions';

const fakeMergeRequests = [
    {
        id: 1,
        title: 'someTitle',
        state: 'opened',
        description: 'someDescription',
        merge_status: 'can_be_merged',
        web_url: 'someUrl',
        created_at: '2019-05-11T14:30:23.406Z',
        updated_at: moment().toISOString(),
        author: {id: 1},
        assignee: {id: 2},
        milestone: {title: 'someTitle', due_date: '2019-05-11T14:30:23.406Z'},
        assignees: [],
        labels: ['some', 'label']
    },
    {
        id: 2,
        title: 'someTitle2',
        state: 'closed',
        description: 'someDescription2',
        merge_status: 'can_be_merged',
        web_url: 'someUrl2',
        created_at: moment().toISOString(),
        updated_at: moment().toISOString(),
        author: {id: 2},
        assignee: {id: 2},
        milestone: {title: 'someTitle2', due_date: '2019-05-11T14:30:23.406Z'},
        assignees: [],
        labels: ['some2', 'label']
    },
    {
        id: 3,
        title: 'someTitle3',
        state: 'merged',
        description: 'someDescription3',
        merge_status: 'can_be_merged',
        web_url: 'someUrl3',
        created_at: moment().toISOString(),
        updated_at: moment().toISOString(),
        author: {id: 3},
        assignee: {id: 2},
        milestone: {title: 'someTitle3', due_date: '2019-05-11T14:30:23.406Z'},
        assignees: [],
        labels: ['some', 'label3']
    }
];

describe('MergeRequests', () => {
    let MergeRequests;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../repository/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        MergeRequests = getRegisteredRoutes().MergeRequests;

        store = require('store').getStore();
        store.dispatch(clearMergeRequests());

        // populate navigation's history
        store.dispatch(navigate('Repository'));
        store.dispatch(navigate('Glasnost'));

        store.dispatch({type: 'REPOSITORY_RECEIVED', data: {id: 1}});
    });

    it('renders correctly the empty page', async () => {
        const component = await asyncCreate(<Provider store={store}><MergeRequests/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should not render because there is no repository', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: null});
        const component = await asyncCreate(<Provider store={store}><MergeRequests/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should not render because there is no mergeRequests', async () => {
        onGet({url: /merge_requests*/, data: null});
        const component = await asyncCreate(<Provider store={store}><MergeRequests/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the list', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = await asyncCreate(<Provider store={store}><MergeRequests/></Provider>);
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders the personal list', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = await asyncCreate(<Provider store={store}><MergeRequests personal={true}/></Provider>);
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('refreshes the list', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = mount(<Provider store={store}><MergeRequests/></Provider>);
        await waitForAsync();
        component.update();

        const ScrollableAppContainer = component.find('ScrollableAppContainerComponent').at(0);
        ScrollableAppContainer.props().refresh();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should ask for more merge_requests when clicking the button', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = mount(<Provider store={store}><MergeRequests/></Provider>);
        await waitForAsync();
        component.update();

        const LoadMore = component.find('LoadMoreComponent');
        LoadMore.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should show the merged mergeRequests', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = mount(<Provider store={store}><MergeRequests/></Provider>);
        await waitForAsync();
        component.update();

        const SingleTab = component.find('SingleTab').at(1);
        SingleTab.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should show the closed mergeRequests', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = mount(<Provider store={store}><MergeRequests/></Provider>);
        await waitForAsync();
        component.update();

        const SingleTab = component.find('SingleTab').at(2);
        SingleTab.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should show the open mergeRequests', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = mount(<Provider store={store}><MergeRequests/></Provider>);
        await waitForAsync();
        component.update();

        const SingleTab = component.find('SingleTab').at(0);
        SingleTab.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should navigate to a single mergeRequest', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = mount(<Provider store={store}><MergeRequests/></Provider>);
        await waitForAsync();
        component.update();

        const SingleMergeRequest = component.find('SingleMergeRequest').at(0).find('TouchableOpacity').at(0);
        SingleMergeRequest.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('MergeRequest');
    });

    it('should navigate to the profile', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = mount(<Provider store={store}><MergeRequests/></Provider>);
        await waitForAsync();
        component.update();

        const Avatar = component.find('SingleMergeRequest').at(0).find('CircledImageComponent').at(0);
        Avatar.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Profile');
    });

    it('should call the functions on componentWillUnmount', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        const component = mount(<Provider store={store}><MergeRequests/></Provider>);

        component.unmount();

        expect(_.isEmpty(getMergedRequests(store.getState()))).toBeTruthy();
    });

    it('renders correctly a lot of open mergeRequests', async () => {
        const mergeRequests = [];

        for (let mrIdx = 0; mrIdx <= 100; mrIdx++) { mergeRequests.push({state: 'opened', id: mrIdx, iid: mrIdx}); }
        onGet({url: /merge_requests*/, data: mergeRequests});
        const component = await asyncCreate(<Provider store={store}><MergeRequests/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders correctly a lot of closed mergeRequests', async () => {
        const mergeRequests = [];

        for (let mrIdx = 0; mrIdx <= 100; mrIdx++) { mergeRequests.push({state: 'closed', id: mrIdx, iid: mrIdx}); }
        onGet({url: /merge_requests*/, data: mergeRequests});
        const component = await asyncCreate(<Provider store={store}><MergeRequests/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders correctly a lot of merged mergeRequests', async () => {
        const mergeRequests = [];

        for (let mrIdx = 0; mrIdx <= 100; mrIdx++) { mergeRequests.push({state: 'merged', id: mrIdx, iid: mrIdx}); }
        onGet({url: /merge_requests*/, data: mergeRequests});
        const component = await asyncCreate(<Provider store={store}><MergeRequests/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('requests the page0', async () => {
        onGet({url: /merge_requests*/, data: fakeMergeRequests});

        store.dispatch(requestMergeRequests(true, true, 1));
        await waitForAsync();

        expect(_.isEmpty(getMergedRequests(store.getState()))).toBeFalsy();
    });

    it('sets the pagination', async () => {
        store.dispatch(setPagination('merge_requests', {'x-page': 1, 'x-per-page': 2}));
        await waitForAsync();

        expect(getPagination(store.getState(), 'merge_requests').pageToLoad).toEqual(1);
        expect(getPagination(store.getState(), 'merge_requests').perPage).toEqual(2);
    });
});
