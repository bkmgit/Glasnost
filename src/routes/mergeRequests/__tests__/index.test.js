import {getRegisteredRoutes} from 'router';
import {getReducers} from 'store';

import * as index from '../index';

describe('MergeRequests index', () => {
    it('should export MergeRequests and reducers', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().MergeRequests).toBeDefined();
        expect(getReducers().mergeRequests).toBeDefined();
    });
});
