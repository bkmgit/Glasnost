import React, {Component} from 'react';
import _ from 'lodash';
import {FlatList, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';

import {CircledImage, FormattedText, Icon, ItemList, ScrollableAppContainer} from 'elements';

import {getMembers} from '../repository/selectors';

function SingleMember({member, onPress}) {
    return (
        <ItemList onPress={() => onPress(member)}>
            <FormattedText>{`${member.name} (@${member.username})`}</FormattedText>
        </ItemList>
    );
}

function Avatars({members, assignees, onAvatarPress}) {
    const users = _.filter(members, member => _.includes(assignees, member.id));

    return (
        <View style={{flexDirection: 'row', justifyContent: 'space-around', flex: 1}}>
            {_.map(users, (assignee, index) => <CircledImage
                key={index}
                name={assignee.name}
                onPress={() => onAvatarPress(assignee)}
                url={assignee.avatar_url}
            />)}
        </View>
    );
}

class AssigneesComponent extends Component {
    state = {isVisible: false};

    render() {
        const {members, assignees, onPress, onAvatarPress} = this.props;
        const {isVisible} = this.state;

        const users = _.filter(members, member => !_.includes(assignees, member.id));

        return (
            <View style={{flexDirection: 'column'}}>
                <TouchableOpacity
                    style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}
                    onPress={() => this.setState({isVisible: !isVisible})}
                >
                    <FormattedText>Assignees: </FormattedText>
                    <Icon name={!isVisible ? 'chevron-down' : 'chevron-up'} size={20}/>
                </TouchableOpacity>

                <Avatars members={members} assignees={assignees} onAvatarPress={onAvatarPress}/>

                {isVisible && <ScrollableAppContainer style={{height: 150}}>
                    {!_.isEmpty(users) && <FlatList
                        data={users}
                        keyExtractor={({name}) => name}
                        renderItem={({item}) => <SingleMember member={item} onPress={onPress}/>}
                    />}
                </ScrollableAppContainer>}
            </View>
        );
    }
}

export const Assignees = connect(state => ({members: getMembers(state)}))(AssigneesComponent);
