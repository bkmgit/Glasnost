import React from 'react';
import {Provider} from 'react-redux';
import {getRegisteredRoutes, getRouteName} from 'router';

const fakeRepository = {id: 1, name: 'someRepoName'};
const fakeMembers = [{id: 1, name: 'someName1', username: 'someUsername1'}, {id: 2, name: 'someName2', username: 'someUsername2'}];

describe('NewIssue', () => {
    let NewIssue;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../repository/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        NewIssue = getRegisteredRoutes().NewIssue;

        store = require('store').getStore();

        onGet({url: /members*/, data: fakeMembers});
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: fakeRepository});
    });

    it('renders correctly the empty page', async () => {
        store.dispatch({type: 'REPOSITORY_RECEIVED', data: null});

        const component = await asyncCreate(<Provider store={store}><NewIssue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders correctly the page', async () => {
        const component = await asyncCreate(<Provider store={store}><NewIssue/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should click the Dismiss button', async () => {
        const component = mount(<Provider store={store}><NewIssue/></Provider>);
        await waitForAsync();
        component.update();

        const MainButton = component.find('MainButton').at(0);
        MainButton.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Repository');
    });

    it("shouldn't click the Submit issue button", async () => {
        const component = mount(<Provider store={store}><NewIssue/></Provider>);
        await waitForAsync();
        component.update();

        const MainButton = component.find('MainButton').at(1);
        MainButton.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('NewIssue');
    });

    it('should write some title', async () => {
        const component = mount(<Provider store={store}><NewIssue/></Provider>);
        await waitForAsync();
        component.update();

        const TextInput = component.find('TextInput').at(0);
        TextInput.props().onChangeText('someTitle');
        TextInput.props().value = 'someTitle';
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should write some description', async () => {
        const component = mount(<Provider store={store}><NewIssue/></Provider>);
        await waitForAsync();
        component.update();

        const TextInput = component.find('TextInput').at(2);
        TextInput.props().onChangeText('someDescription');
        TextInput.props().value = 'someDescription';
        component.update();

        expect(component.html()).toMatchSnapshot();
    });


    it('should write some title and desc and click the button', async () => {
        const component = mount(<Provider store={store}><NewIssue/></Provider>);
        await waitForAsync();
        component.update();

        const TitleInput = component.find('TextInput').at(0);
        TitleInput.props().onChangeText('someTitle');
        TitleInput.props().value = 'someTitle';
        const DescriptionInput = component.find('TextInput').at(2);
        DescriptionInput.props().onChangeText('someDescription');
        DescriptionInput.props().value = 'someDescription';
        component.update();

        const MainButton = component.find('MainButton').at(1);
        MainButton.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should show some users', async () => {
        const component = mount(<Provider store={store}><NewIssue/></Provider>);
        await waitForAsync();
        component.update();

        const TouchableOpacity = component.find('TouchableOpacity').at(0);
        TouchableOpacity.props().onPress();

        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should show some assignee', async () => {
        const component = mount(<Provider store={store}><NewIssue/></Provider>);
        await waitForAsync();
        component.update();

        const TouchableOpacity = component.find('TouchableOpacity').at(0);
        TouchableOpacity.props().onPress();

        component.update();

        const SingleMember = component.find('AssigneesComponent').find('SingleMember').at(0).find('ItemListComponent');
        SingleMember.props().onPress();

        component.update();

        SingleMember.props().onPress();

        expect(component.html()).toMatchSnapshot();
    });

    it('should click on some Avatar', async () => {
        const component = mount(<Provider store={store}><NewIssue/></Provider>);
        await waitForAsync();
        component.update();

        const TouchableOpacity = component.find('TouchableOpacity').at(0);
        TouchableOpacity.props().onPress();

        component.update();

        const SingleMember = component.find('AssigneesComponent').find('SingleMember').at(0).find('ItemListComponent');
        SingleMember.props().onPress();

        component.update();

        const Avatar = component.find('Avatars').find('CircledImageComponent').at(0);
        Avatar.props().onPress();

        component.update();

        Avatar.props().onPress();

        expect(component.html()).toMatchSnapshot();
    });
});
