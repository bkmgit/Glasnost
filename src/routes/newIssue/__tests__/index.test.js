import {getRegisteredRoutes} from 'router';

import * as index from '../index';

describe('NewIssue index', () => {
    it('should export NewIssue', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().NewIssue).toBeDefined();
    });
});
