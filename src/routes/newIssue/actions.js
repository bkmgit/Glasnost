import {post} from 'api';

import {getRepository} from '../repository/selectors';


export function createNewIssue({title, description, assignees}) {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        if (!repo || !title) {
            return null;
        }

        await post({path: `projects/${repo.id}/issues`, body: {title, description, assignee_ids: assignees}});
    };
}
