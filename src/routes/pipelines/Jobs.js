import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {navigate, registerRoute} from 'router';
import {Card, FormattedText} from 'elements';

import {View} from 'react-native';
import {colors} from 'elements/styles';
import {clearPipelines} from './actions';
import {getJobs} from './selectors';
import {StatusIndicator} from './StatusIndicator';

const expandedStageStyle = {
    flexDirection: 'row',
    marginHorizontal: 20,
    alignItems: 'center',
    marginVertical: 5,
    justifyContent: 'space-around'
};

function ExpandedView({isExpanded, jobs, navigate}) {
    if (!isExpanded) {
        return null;
    }

    const mappedJobs = _.map(jobs, (job, index) => (
        <View style={expandedStageStyle} key={index}>
            <FormattedText>{job.name || ''}</FormattedText>
            <StatusIndicator key={index} status={job.status} onPress={() => navigate('Trace', {job})}/>
        </View>
    ));

    return (
        <View style={{marginTop: 10}}>
            {mappedJobs}
        </View>
    );
}

class JobsComponent extends Component {
    state = {isExpanded: false};

    componentWillUnmount() {
        this.props.clearPipelines();
    }

    render() {
        const {jobs, navigate} = this.props;
        const {isExpanded, jobsIndex} = this.state;
        const jobsByGroup = _.groupBy(jobs, 'stage');

        if (!jobs) {
            return null;
        }

        return (
            <Card title={'Jobs'} icon={{name: 'pipeline'}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-around', flexWrap: 'wrap'}}>
                    {_.map(jobsByGroup, (jobs, index) => (
                        <View
                            key={index}
                            style={{
                                backgroundColor: jobsIndex === index ? colors.itemListDarkTheme : 'transparent',
                                borderRadius: 5,
                                padding: 5
                            }}
                        >
                            <StatusIndicator
                                status={_.last(jobs).status}
                                onPress={() => {
                                    if (!isExpanded) {
                                        this.setState({isExpanded: true, jobsIndex: index});
                                    } else if (jobsIndex !== index) {
                                        this.setState({jobsIndex: index});
                                    } else {
                                        this.setState({isExpanded: false});
                                    }
                                }}
                            />
                        </View>
                    ))}
                </View>
                <ExpandedView isExpanded={isExpanded} jobs={jobsByGroup[jobsIndex]} navigate={navigate}/>
            </Card>
        );
    }
}

export const Jobs = connect((state, {pipelineId}) => ({jobs: getJobs(state, pipelineId)}), {clearPipelines, navigate})(JobsComponent);
registerRoute({Jobs});
