import {colors, Icon} from 'elements';
import {View} from 'react-native';
import React from 'react';

function getStyleOnStatus(status) {
    const mappings = {
        running: {color: colors.lightBlue, icon: 'status_running_borderless'},
        pending: {color: colors.strangeOrange, icon: 'status_pending_borderless'},
        success: {color: colors.success, icon: 'status_success_borderless'},
        failed: {color: colors.warning, icon: 'status_failed_borderless'},
        canceled: {color: colors.darkerBorder, icon: 'status_canceled_borderless'},
        skipped: {color: colors.lighGray, icon: 'status_skipped_borderless'},
        created: {color: colors.lighGray, icon: 'status_skipped_borderless'},
        manual: {color: colors.border, icon: 'status_manual_borderless'},
        warning: {color: colors.gray1, icon: 'status_warning_borderless'},
        scheduled: {color: colors.border, icon: 'status_scheduled_borderless'},
        notfound: {color: colors.border, icon: 'status_notfound_borderless'}
    };

    return mappings[status];
}

function getContainerStyle(color) {
    return {
        height: 25,
        width: 25,
        borderRadius: 25,
        borderColor: color,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    };
}

export function StatusIndicator({status, onPress}) {
    const {color, icon} = getStyleOnStatus(status);

    return (
        <View style={getContainerStyle(color)}>
            <Icon name={icon} color={color} size={20} onPress={onPress}/>
        </View>
    );
}
