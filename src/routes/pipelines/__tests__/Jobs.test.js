import React from 'react';
import {Provider} from 'react-redux';
import {getRegisteredRoutes, getRouteName} from 'router';
import _ from 'lodash';
import {getJobs} from '../selectors';
import {clearPipelines, requestJobsForPipeline, requestPipelines, requestPipelinesForBranch} from '../actions';
import {setLiteExperience} from '../../settings/actions';

const fakeJobs = [
    {projectId: 1, pipelineId: 1, status: 'success', stage: 'someStage'},
    {projectId: 1, pipelineId: 2, status: 'running', stage: 'someStage2'}
];

const fakePipelines = [{some: 'pipeline'}, {some2: 'pipeline2'}];
const fakeRepos = [{id: 1}, {id: 2}];

describe('Jobs', () => {
    let Jobs;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../settings/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        Jobs = getRegisteredRoutes().Jobs;

        store = require('store').getStore();

        store.dispatch(clearPipelines());
        store.dispatch({type: 'JOBS_RECEIVED', pipelineId: 1, data: fakeJobs});
    });

    it('renders correctly the empty page', async () => {
        const component = await asyncCreate(<Provider store={store}><Jobs/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });


    it('should call the functions on unmount', async () => {
        const component = mount(<Provider store={store}><Jobs/></Provider>);

        component.unmount();
        await waitForAsync();

        expect(_.isEmpty(store.getState().pipelines.pipelines)).toBeTruthy();
    });

    it('renders some job', async () => {
        const component = await asyncCreate(<Provider store={store}><Jobs pipelineId={1}/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should click on the first StatusIndicator', async () => {
        const component = mount(<Provider store={store}><Jobs pipelineId={1}/></Provider>);
        await waitForAsync();
        component.update();

        const StatusIndicator = component.find('StatusIndicator').at(0).find('Icon');
        StatusIndicator.props().onPress();
        component.update();

        StatusIndicator.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should click on the second StatusIndicator', async () => {
        const component = mount(<Provider store={store}><Jobs pipelineId={1}/></Provider>);
        await waitForAsync();
        component.update();

        const StatusIndicator = component.find('StatusIndicator').at(1).find('Icon');
        StatusIndicator.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('clicking on the ExpandedView should navigate to the Trace page', async () => {
        const component = mount(<Provider store={store}><Jobs pipelineId={1}/></Provider>);
        await waitForAsync();
        component.update();

        const StatusIndicator = component.find('StatusIndicator').at(0).find('Icon');
        StatusIndicator.props().onPress();
        component.update();

        const ExpandedView = component.find('ExpandedView').find('StatusIndicator').at(0).find('Icon');
        ExpandedView.props().onPress();
        component.update();

        expect(getRouteName(store.getState())).toEqual('Trace');
    });

    it('should not requestJobsForPipeline because the pipelineId is missing', async () => {
        store.dispatch(requestJobsForPipeline());

        global.onGet = jest.fn();

        expect(global.onGet).toHaveBeenCalledTimes(0);
    });

    it('should not requestJobsForPipeline because the job is already existing', async () => {
        global.onGet = jest.fn();

        store.dispatch(requestJobsForPipeline(1, 1, false));
        await waitForAsync();

        expect(global.onGet).toHaveBeenCalledTimes(0);
    });

    it('should continue because the pipelineId is not existing', async () => {
        global.onGet = jest.fn();

        store.dispatch(requestJobsForPipeline(1, 4, false));
        await waitForAsync();

        expect(global.onGet).toHaveBeenCalledTimes(0);
    });

    it('should requestJobsForPipeline', async () => {
        onGet({url: /jobs*/, data: fakeJobs});

        store.dispatch(requestJobsForPipeline(1, 1, true));
        await waitForAsync();

        expect(_.isEmpty(getJobs(store.getState(), 1))).toBeFalsy();
    });


    it('should not requestJobsForPipeline because there is no data', async () => {
        onGet({url: /jobs*/, data: []});

        store.dispatch(requestJobsForPipeline(1, 1, true));
        await waitForAsync();

        expect(_.isEmpty(getJobs(store.getState(), 1))).toBeFalsy();
    });

    it('should not requestPipelinesForBranch because isLite', async () => {
        global.onGet = jest.fn();

        store.dispatch(setLiteExperience('true'));
        await waitForAsync();

        store.dispatch(requestPipelinesForBranch(1, 'someBranch'));
        await waitForAsync();

        expect(global.onGet).toHaveBeenCalledTimes(0);
    });

    it('should not requestPipelinesForBranch because there is no data', async () => {
        onGet({url: /pipelines*/, data: []});

        store.dispatch(setLiteExperience('false'));
        await waitForAsync();

        store.dispatch(requestPipelinesForBranch(1, 'someBranch'));
        await waitForAsync();

        expect(_.isEmpty(store.getState().pipelines.pipelines)).toBeTruthy();
    });

    it('should requestPipelinesForBranch because not isLite', async () => {
        onGet({url: /pipelines*/, data: fakePipelines});

        store.dispatch(setLiteExperience('false'));
        await waitForAsync();

        store.dispatch(requestPipelinesForBranch(1, 'someBranch'));
        await waitForAsync();

        expect(_.isEmpty(store.getState().pipelines.pipelines)).toBeFalsy();
    });

    it('should requestPipelines because is not isLite', async () => {
        onGet({url: /pipelines*/, data: fakePipelines});

        store.dispatch(setLiteExperience('false'));
        await waitForAsync();

        store.dispatch(requestPipelines(fakeRepos));
        await waitForAsync();

        expect(_.isEmpty(store.getState().pipelines.pipelines)).toBeFalsy();
    });

    it('should not requestPipelines because isLite', async () => {
        global.onGet = jest.fn();

        store.dispatch(setLiteExperience('true'));
        await waitForAsync();

        store.dispatch(requestPipelines(fakeRepos));
        await waitForAsync();

        expect(global.onGet).toHaveBeenCalledTimes(0);
    });

    it('should not requestPipelines because is there is no data', async () => {
        onGet({url: /pipelines*/, data: []});

        store.dispatch(setLiteExperience('false'));
        await waitForAsync();

        store.dispatch(requestPipelines(fakeRepos));
        await waitForAsync();

        expect(_.isEmpty(store.getState().pipelines.pipelines)).toBeTruthy();
    });
});
