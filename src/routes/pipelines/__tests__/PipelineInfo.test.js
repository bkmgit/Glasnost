import React from 'react';
import {Provider} from 'react-redux';

const fakeRepository = {id: 1, default_branch: 'someBranch'};

describe('PipelineInfo', () => {
    let PipelineInfo;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        require('../../settings/reducers');
        setStore(getStore());
    });

    beforeEach(() => {
        PipelineInfo = require('../PipelineInfo').PipelineInfo;

        store = require('store').getStore();
    });

    it('renders correctly the page', async () => {
        const component = await asyncCreate(
                                                <Provider store={store}>
                                                    <PipelineInfo repository={fakeRepository} branch={'someBranch'}/>
                                                </Provider>
        );

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('calls onPress', async () => {
        store.dispatch({type: 'PIPELINES_RECEIVED', projectId: 1, data: [{ref: 'someBranch', id: 1, status: 'running'}]});
        await waitForAsync();

        const onPress = jest.fn();

        const component = mount(
                                                <Provider store={store}>
                                                    <PipelineInfo repository={fakeRepository} branch={'someBranch'} onPress={onPress}/>
                                                </Provider>
        );
        await waitForAsync();
        component.update();

        const StatusIndicator = component.find('StatusIndicator');
        StatusIndicator.props().onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });
});
