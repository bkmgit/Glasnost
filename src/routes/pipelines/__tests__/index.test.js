import {getRegisteredRoutes} from 'router';
import {getReducers} from 'store';

import * as index from '../index';

describe('Pipelines index', () => {
    it('should export Pipelines', () => {
        expect(index.Jobs).toBeDefined();
        expect(getRegisteredRoutes().Jobs).toBeDefined();
        expect(getReducers().pipelines).toBeDefined();
    });
});
