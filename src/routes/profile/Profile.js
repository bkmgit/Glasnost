import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';
import moment from 'moment';

import {FormattedText, ScrollableAppContainer, AppContainer, CircledImage} from 'elements';
import {registerRoute} from 'router';
import {getOtherGuy} from './selectors';
import {requestUser} from './actions';
import {Events} from './Events';

class ProfileComponent extends Component {
    componentDidMount() {
        const {requestUser, id} = this.props;

        if (id) {
            return requestUser(id);
        }
    }

    render() {
        const {profile, requestUser, id} = this.props;

        if (_.isEmpty(profile)) {
            return <AppContainer/>;
        }

        return (
            <ScrollableAppContainer refresh={() => requestUser(id)}>
                <Header profile={profile}/>
                <Events id={id}/>
            </ScrollableAppContainer>
        );
    }
}

function Header({profile}) {
    const {name, username, created_at, bio, avatar_url} = profile;

    return (
        <View style={{alignItems: 'center', margin: 10}}>
            <FormattedText style={{fontSize: 20, fontWeight: 'bold', marginBottom: 10}}>{name}</FormattedText>
            <CircledImage name={name} url={avatar_url} size={70}/>
            <View style={{flexDirection: 'row', marginVertical: 10}}>
                <FormattedText style={{fontSize: 14, fontStyle: 'italic', marginRight: 10}}>
                    {`@${username}`}
                </FormattedText>
                <FormattedText style={{fontSize: 14, fontStyle: 'italic'}}>
                    {`Member since ${moment(created_at).format('MMM DD, YYYY')}`}
                </FormattedText>
            </View>
            {!!bio && <FormattedText style={{fontSize: 14, fontStyle: 'italic'}}>{bio}</FormattedText>}
        </View>
    );
}

const Profile = connect(state => ({profile: getOtherGuy(state)}), {requestUser})(ProfileComponent);
registerRoute({Profile});
