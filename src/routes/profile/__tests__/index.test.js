import {getRegisteredRoutes} from 'router';

import * as index from '../index';

describe('Profile index', () => {
    it('should export Profile', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().Profile).toBeDefined();
    });
});
