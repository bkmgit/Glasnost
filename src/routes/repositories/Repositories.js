import React, {memo, Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {registerRoute} from 'router';
import {getData} from 'utils';

import {RepositoriesList} from './RepositoriesList';
import {clearRepositories, requestRepositories} from './actions';
import {getRepositories} from './selectors';
import {clearPipelines} from '../pipelines/actions';
import {clearMergeRequests} from '../mergeRequests/actions';

class RepositoriesComponent extends Component {
    state = {persistedRepositories: null};

    async componentDidMount() {
        const persistedRepositories = _.get(await getData(), 'repositories');
        this.setState({persistedRepositories});
        this.props.requestRepositories();
    }

    refresh() {
        this.props.clearRepositories();
        this.props.requestRepositories(true);
        this.props.clearPipelines();
    }

    componentWillUnmount() {
        this.props.clearMergeRequests();
    }

    render() {
        const {repositories, requestRepositories} = this.props;
        const {persistedRepositories} = this.state;

        return <RepositoriesList
            entityName={'repositories'}
            repositories={repositories || persistedRepositories}
            refresh={() => this.refresh()}
            onPress={() => requestRepositories(false)}
        />;
    }
}


const Repositories = connect(state => ({
    repositories: getRepositories(state)
}), {requestRepositories, clearRepositories, clearPipelines, clearMergeRequests})(memo(RepositoriesComponent));
registerRoute({Repositories});
