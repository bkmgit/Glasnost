import React, {memo} from 'react';
import {connect} from 'react-redux';

import {navigate} from 'router';
import {CircledImage, ItemList} from 'elements';

import {Description} from './Description';

function SingleRepositoryComponent({repo, navigate}) {
    const {avatar_url, name, id} = repo;

    return (
        <ItemList onPress={() => navigate('Repository', {id})} style={{padding: 5}}>
            <CircledImage url={avatar_url} name={name}/>
            <Description repository={repo}/>
        </ItemList>
    );
}

export const SingleRepository = connect(null, {navigate})(memo(SingleRepositoryComponent));
