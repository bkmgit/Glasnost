import React, {memo, Component} from 'react';
import {connect} from 'react-redux';

import {registerRoute} from 'router';
import {RepositoriesList} from './RepositoriesList';
import {clearStarredRepositories, requestStarredRepositories} from './actions';
import {getStarredRepositories} from './selectors';
import {clearPipelines} from '../pipelines/actions';

class StarredRepositoriesComponent extends Component {
    componentDidMount() {
        this.props.requestStarredRepositories();
    }

    refresh() {
        this.props.clearStarredRepositories();
        this.props.requestStarredRepositories(true);
        this.props.clearPipelines();
    }

    componentWillUnmount() {
        this.props.clearStarredRepositories();
    }

    render() {
        const {repositories, requestStarredRepositories} = this.props;

        return <RepositoriesList
            entityName={'starredRepositories'}
            repositories={repositories}
            refresh={() => this.refresh()}
            onPress={() => requestStarredRepositories(false)}
            searchRemote={false}
        />;
    }
}


const StarredRepositories = connect(state => ({
    repositories: getStarredRepositories(state)
}), {requestStarredRepositories, clearStarredRepositories, clearPipelines})(memo(StarredRepositoriesComponent));
registerRoute({StarredRepositories});
