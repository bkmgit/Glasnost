import React from 'react';
import {Provider} from 'react-redux';

import {getRegisteredRoutes} from 'router';
import {getRepositories} from '../selectors';
import {clearRepositories} from '../actions';

const fakeRepositories = [
    {id: 1},
    {id: 2}
];

describe('Repositories', () => {
    let Repositories;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        setStore(getStore());
    });

    beforeEach(() => {
        Repositories = getRegisteredRoutes().Repositories;

        store = require('store').getStore();
        store.dispatch({type: 'REPOSITORIES_RECEIVED', data: fakeRepositories});
    });

    it('renders correctly the empty page', async () => {
        store.dispatch(clearRepositories());
        const component = await asyncCreate(<Provider store={store}><Repositories/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders correctly the page', async () => {
        const component = await asyncCreate(<Provider store={store}><Repositories/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should call the functions when unmounting', async () => {
        const component = mount(<Provider store={store}><Repositories/></Provider>);

        component.unmount();
        await waitForAsync();

        expect(getRepositories(store.getState())).toMatchSnapshot();
    });

    it('should refresh the data', async () => {
        const component = mount(<Provider store={store}><Repositories/></Provider>);
        await waitForAsync();
        component.update();

        const RepositoriesList = component.find('RepositoriesListComponent');
        RepositoriesList.props().refresh();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should request the repositories when onPress', async () => {
        const component = mount(<Provider store={store}><Repositories/></Provider>);
        await waitForAsync();
        component.update();

        const RepositoriesList = component.find('RepositoriesListComponent');
        RepositoriesList.props().onPress();
        await waitForAsync();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });
});
