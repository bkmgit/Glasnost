import moment from 'moment';
import _ from 'lodash';
import {get} from 'api';
import {getPagination, setPagination, showDonationPopup, showStarsPopup} from 'utils';
import {getData, setData} from 'utils/storage';

import {lastSearchOnline} from './selectors';
import {requestPipelines} from '../pipelines/actions';

export function clearRepositories() {
    return {type: 'CLEAR_REPOSITORIES'};
}

export function clearOnlineRepositories() {
    return {type: 'CLEAR_ONLINE_REPOSITORIES'};
}

export function clearStarredRepositories() {
    return {type: 'CLEAR_STARRED_REPOSITORIES'};
}

export function setLocalSearch(localSearch) {
    return {type: 'SET_LOCAL_SEARCH', localSearch};
}

function setLastSearchOnline(lastSearchOnline) {
    return {type: 'SET_LAST_SEARCH_ONLINE', lastSearchOnline};
}

export function requestOnlineRepositories(text) {
    return async (dispatch, getState) => {
        const state = getState();
        const search = text || lastSearchOnline(state);
        const {pageToLoad = 0} = getPagination(state, 'onlineRepositories');

        const {data, headers} = await get({
            page: pageToLoad,
            path: 'search',
            additionalParams: {scope: 'projects', search}
        });

        if (data) {
            dispatch({type: 'ONLINE_REPOSITORIES_RECEIVED', data});
            dispatch(requestPipelines(data));
            dispatch(setPagination('onlineRepositories', headers));
        }
    };
}

export function onlineSearch(text) {
    return async (dispatch, getState) => {
        if (!_.size(text)) {
            return null;
        }

        const lastSearchedText = lastSearchOnline(getState());
        const newText = lastSearchedText !== text;

        if (newText) {
            dispatch(clearOnlineRepositories());
        }

        dispatch(setLastSearchOnline(text));
        dispatch(requestOnlineRepositories(text));
    };
}

export function requestRepositories(page0) {
    return async (dispatch, getState) => {
        const {pageToLoad = 0} = getPagination(getState(), 'repositories');

        const {data, headers} = await get({
            page: page0 ? 0 : pageToLoad,
            path: 'projects',
            orderBy: 'last_activity_at',
            additionalParams: {archived: false, min_access_level: 10, per_page: 10},
            cached: !page0,
            loader: false
        });

        if (!_.isEmpty(data)) {
            dispatch({type: 'REPOSITORIES_RECEIVED', data});
            dispatch(setPagination('repositories', headers));
            dispatch(requestPipelines(data));
            setData('repositories', data);
        }
    };
}

export function requestStarredRepositories(page0) {
    return async (dispatch, getState) => {
        const {pageToLoad = 0} = getPagination(getState(), 'starredRepositories');

        const {data, headers} = await get({
            page: page0 ? 0 : pageToLoad,
            path: 'projects',
            orderBy: 'last_activity_at',
            additionalParams: {starred: true, archived: false, min_access_level: 10},
            cached: !page0
        });

        if (!_.isEmpty(data)) {
            dispatch({type: 'STARRED_REPOSITORIES_RECEIVED', data});
            dispatch(setPagination('starredRepositories', headers));
            dispatch(requestPipelines(data));
        }
    };
}

export function checkDonation() {
    return async (dispatch) => {
        const lastCheck = _.get(await getData(), 'lastDonationCheck');

        if (!lastCheck) {
            return setData('lastDonationCheck', moment().toISOString());
        }

        if (moment(lastCheck).isAfter(moment().subtract(15, 'days'))) {
            return null;
        }

        await setData('lastDonationCheck', moment().toISOString());
        dispatch(showDonationPopup());
    };
}


export function checkStars() {
    return async (dispatch) => {
        const lastCheck = _.get(await getData(), 'lastStarsCheck');

        if (!lastCheck) {
            return setData('lastStarsCheck', moment().toISOString());
        }

        if (moment(lastCheck).isAfter(moment().subtract(10, 'days'))) {
            return null;
        }

        await setData('lastStarsCheck', moment().toISOString());
        dispatch(showStarsPopup());
    };
}
