import _ from 'lodash';
import {createSelector} from 'reselect';

const getRepositoriesSelector = state => _.get(state, 'repositories');
export const getRepositories = createSelector(getRepositoriesSelector, repositories => _.get(repositories, 'repositories'));
export const isLocalSearch = createSelector(getRepositoriesSelector, repositories => _.get(repositories, 'localSearch', true));
export const getOnlineRepositories = state => _.get(state, ['onlineRepositories', 'onlineRepositories']);
export const getStarredRepositories = state => _.get(state, ['starredRepositories', 'starredRepositories']);
export const lastSearchOnline = state => _.get(state, ['onlineRepositories', 'lastSearchOnline'], '');
