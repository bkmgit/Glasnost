import React, {Component} from 'react';
import {connect} from 'react-redux';

import {AppContainer, CodeDisplayer, Markdown, ScrollableAppContainer} from 'elements';
import {registerRoute} from 'router';
import {setTitle} from 'headerFooter';
import {getBlob, getBlobName} from './selectors';

class BlobViewerComponent extends Component {
    componentDidMount() {
        this.props.setTitle(this.props.blobName);
    }

    render() {
        const {blob, blobName} = this.props;

        if (!blobName || !blob.blob) {
            return <AppContainer/>;
        }

        const extension = blobName ? blobName.substr(blobName.lastIndexOf('.') + 1) : '';

        if (extension === 'md') {
            return (
                <ScrollableAppContainer>
                    <Markdown text={blob.blob}/>
                </ScrollableAppContainer>
            );
        }
        return <CodeDisplayer language={extension}>{blob.blob}</CodeDisplayer>;
    }
}

const BlobViewer = connect(state => ({
    blob: getBlob(state),
    blobName: getBlobName(state)
}), {setTitle})(BlobViewerComponent);
registerRoute({BlobViewer});
