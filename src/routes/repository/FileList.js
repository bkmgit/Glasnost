import React, {Component} from 'react';
import {FlatList} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';

import {navigate, registerRoute} from 'router';
import {FormattedText, ItemList, Icon, AppContainer} from 'elements';
import {setFooter, setHeaderLeftButton} from 'headerFooter';

import {clearFilelist, fetchBlob, fetchSubPath, requestFileList} from './actions';
import {getFileList} from './selectors';
import {Footer} from './Footer';
import {getDefaultBranchName} from '../branches/selectors';

function getFileIcon({type}) {
    const typeMappings = {
        blob: 'doc-text',
        tree: 'folder-o'
    };

    return typeMappings[type] || typeMappings.blob;
}

function removeLastPath(path) {
    return path.substr(0, path.lastIndexOf('/'));
}

class FileListComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {currentBranch: props.branch};
    }

    headerLeftButton() {
        return <Icon name={'angle-left'} size={25} style={{padding: 5}} onPress={() => this.props.navigate('Repository')}/>;
    }

    footer() {
        return <Footer/>;
    }

    onPressAction({type, path, name, id}) {
        const {navigate, fetchSubPath, fetchBlob} = this.props;

        if (type === 'tree') {
            return fetchSubPath(path);
        }

        if (type === 'blob') {
            fetchBlob(id, name);

            return navigate('BlobViewer');
        }

        return {};
    }

    componentDidMount() {
        const {requestFileList, setFooter, setHeaderLeftButton} = this.props;

        requestFileList();
        setHeaderLeftButton(this.headerLeftButton());
        setFooter(this.footer());
    }

    componentDidUpdate() {
        const {currentBranch} = this.state;
        const {branch, requestFileList} = this.props;

        if (branch && currentBranch !== branch) {
            this.setState({currentBranch: branch});
            requestFileList();
        }
    }

    componentWillUnmount() {
        this.props.clearFilelist();
    }

    onScroll({nativeEvent}) {
        const {setFooter} = this.props;
        const currentOffset = nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);

        if (dif < 0) {
            setFooter(this.footer());
        } else {
            setFooter();
        }

        this.offset = currentOffset;
    }

    render() {
        const {fileList, subPathList, fetchSubPath} = this.props;
        const list = !_.isEmpty(subPathList) ? subPathList : fileList;

        const {path} = _.get(list, '0', {});
        const isSubPath = _.includes(path, '/');
        const previousPath = isSubPath ? removeLastPath(path) : '';

        return (
            <AppContainer>
                {isSubPath
                    && <ItemList
                        style={{height: 40, marginBottom: 5}}
                        onPress={() => fetchSubPath(_.includes(previousPath, '/') ? removeLastPath(previousPath) : '')}
                    >
                        <FormattedText>{'..'}</FormattedText>
                    </ItemList>
                }
                <FlatList
                    data={list}
                    keyExtractor={({id}) => id}
                    onScroll={e => this.onScroll(e)}
                    renderItem={({item}) => <SingleFile file={item} onPressAction={file => this.onPressAction(file)}/>}
                />
            </AppContainer>
        );
    }
}

function SingleFile({file, onPressAction}) {
    return (
        <ItemList style={{height: 40}} onPress={() => onPressAction(file)}>
            <Icon style={{marginRight: 7}} name={getFileIcon(file)} size={20}/>
            <FormattedText>{file.name}</FormattedText>
        </ItemList>
    );
}

const FileList = connect(state => ({
    fileList: getFileList(state),
    branch: getDefaultBranchName(state)
}), {
    fetchSubPath, fetchBlob, requestFileList, clearFilelist, setFooter, setHeaderLeftButton, navigate
})(FileListComponent);
registerRoute({FileList});
