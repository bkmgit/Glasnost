import React from 'react';
import _ from 'lodash';
import {connect} from 'react-redux';
import {TouchableOpacity, View} from 'react-native';

import {FormattedText, Icon, MainButton} from 'elements';
import {hidePopup, showPopup} from 'utils';
import {get} from 'api';

import {forkProject} from './actions';
import {getRepository} from './selectors';
import {getMe} from '../profile/selectors';

export function forkProjectPopup() {
    return async (dispatch, getState) => {
        const state = getState();
        const repo = getRepository(state);

        if (!repo) {
            return null;
        }

        const me = getMe(state);
        const namespaces = await get({path: 'namespaces'});
        const {data} = await get({path: `projects/${repo.id}/forks`});
        const originId = _.get(_.first(data), 'forked_from_project.namespace.id');
        const forkedIds = _.map(data, 'id');
        const cleanNamespaces = _.filter(_.get(namespaces, 'data'), namespace => !_.includes(forkedIds, namespace.id) && namespace.id !== originId);

        const groupsWithPermissions = [];
        for (const namespace of cleanNamespaces) {
            const {data} = await get({path: `groups/${namespace.id}/members/${me.id}`, loader: false});
            if (data.access_level > 30 && data.state === 'active') {
                groupsWithPermissions.push(namespace.id);
            }
        }
        const validGroups = _.filter(cleanNamespaces, namespace => _.includes(groupsWithPermissions, namespace.id));

        if (validGroups) {
            dispatch(showPopup({
                title: 'To which namespace do you want to fork the project?',
                customComponent: <Namespaces namespaces={validGroups}/>,
                buttons: [<MainButton key={'Dismiss'} text={'Dismiss'} onPress={() => dispatch(hidePopup())}/>]
            }));
        }
    };
}


function NamespacesComponent({namespaces, forkProject, hidePopup}) {
    if (_.isEmpty(namespaces)) {
        return null;
    }

    return (
        <View>
            {_.map(namespaces, (namespace, index) => (
                <TouchableOpacity
                    key={index}
                    onPress={() => {
                        forkProject(namespace.id);
                        hidePopup();
                    }}
                    style={{marginBottom: 3, flexDirection: 'row', alignItems: 'center'}}
                >
                    <Icon name={'angle-right'} size={25}/>
                    <FormattedText style={{fontSize: 18, marginLeft: 10}}>{namespace.full_path}</FormattedText>
                </TouchableOpacity>
            ))}
        </View>
    );
}

const Namespaces = connect(null, {forkProject, hidePopup})(NamespacesComponent);
