import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    ScrollableAppContainer,
    AppContainer,
    Icon
} from 'elements';
import {navigate, registerRoute} from 'router';
import {setFooter, setHeaderLeftButton, setPlusButton, setTitle} from 'headerFooter';

import {requestRepository} from './actions';
import {getRepository} from './selectors';
import {clearMergeRequests} from '../mergeRequests/actions';
import {clearIssues} from '../issues/actions';
import {Footer} from './Footer';
import {Stats} from './Stats';
import {Readme} from './Readme';
import {Monitor} from './Monitor';
import {PlusButton} from './PlusButton';
import {Header} from './Header';

class RepositoryComponent extends Component {
    componentDidMount() {
        this.setInitialState();
    }

    setInitialState() {
        const {requestRepository, setHeaderLeftButton, setFooter, setPlusButton, setTitle, id} = this.props;

        setTitle('Project');
        setHeaderLeftButton(this.headerLeftButton());
        setFooter(this.footer());
        setPlusButton(this.plusButton());
        requestRepository(id);
    }

    componentDidUpdate(oldProps) {
        if (this.props.id !== oldProps.id) {
            this.setInitialState();
            return true;
        }
    }

    headerLeftButton() {
        return <Icon name={'angle-left'} size={25} style={{padding: 5}} onPress={() => this.props.navigate('Repositories')}/>;
    }

    footer() {
        return <Footer/>;
    }


    plusButton() {
        return <PlusButton/>;
    }

    render() {
        const {repository, requestRepository} = this.props;

        if (!repository) {
            return <AppContainer/>;
        }

        return (
            <ScrollableAppContainer refresh={() => requestRepository(repository.id)}>
                <Header/>
                <Stats/>
                <Monitor/>
                <Readme/>
            </ScrollableAppContainer>
        );
    }
}

const Repository = connect(state => ({repository: getRepository(state)}), {
    requestRepository,
    clearMergeRequests,
    clearIssues,
    setFooter,
    setPlusButton,
    setHeaderLeftButton,
    navigate,
    setTitle
})(RepositoryComponent);
registerRoute({Repository});
