import {View} from 'react-native';
import React, {Component, memo} from 'react';
import _ from 'lodash';
import {connect} from 'react-redux';

import {getProjectPermission, numberToHumanSize} from 'utils';
import {Card, FormattedText, Icon, Tag} from 'elements';

import {PipelineInfo} from '../pipelines/PipelineInfo';
import {Jobs} from '../pipelines';
import {getRepository} from './selectors';
import {getDefaultBranchName} from '../branches/selectors';
import {getPipeline} from '../pipelines/selectors';
import {requestJobsForPipeline} from '../pipelines/actions';

function Stars({repository}) {
    const {star_count} = repository;

    return (
        <View style={{
            flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginVertical: 10
        }}>
            <Icon name={'star'} size={30}/>
            <FormattedText>{star_count}</FormattedText>
        </View>
    );
}

class StatsComponent extends Component {
    state = {areJobsVisible: false};

    onPipelinePress(pipeline) {
        const {areJobsVisible} = this.state;
        const {repository, requestJobsForPipeline} = this.props;

        if (!areJobsVisible) {
            requestJobsForPipeline(repository.id, pipeline.id);
        }

        this.setState({areJobsVisible: !areJobsVisible});
    }

    render() {
        const {repository, branch, getPipeline} = this.props;
        const pipeline = getPipeline(repository.id, branch);

        if (_.isEmpty(repository.statistics)) {
            return null;
        }

        const {permissions, statistics: {repository_size, lfs_objects_size}, visibility, forks_count} = repository;
        const project_access = _.get(permissions, 'project_access', {});
        const group_access = _.get(permissions, 'group_access', {});
        const permission = getProjectPermission(project_access, group_access);
        const {areJobsVisible} = this.state;

        return (
            <Card title={'Stats'} icon={{name: 'overview'}}>
                <View style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    flex: 1,
                    justifyContent: 'space-between'
                }}>
                    <FormattedText>{`Files: ${numberToHumanSize(repository_size + lfs_objects_size)}`}</FormattedText>
                    <View style={{flexDirection: 'row'}}>
                        <FormattedText style={{marginRight: 5}}>Visibility: </FormattedText>
                        <Icon name={visibility === 'private' ? 'lock' : 'lock-open'} size={20}/>
                    </View>
                    {!!permission && <Tag text={permission}/>}
                </View>
                <View style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flexDirection: 'row',
                    marginTop: 20
                }}>
                    {branch
                    && <View style={{flexDirection: 'row'}}>
                        <FormattedText>Current branch: </FormattedText>
                        <FormattedText style={{fontWeight: 'bold'}}>{branch}</FormattedText>
                    </View>}
                    <PipelineInfo repository={repository} branch={branch} onPress={() => this.onPipelinePress(pipeline)}/>
                    <View style={{flexDirection: 'row', marginLeft: 10}}>
                        <FormattedText>{`Forks: ${forks_count}`}</FormattedText>
                    </View>
                </View>

                {areJobsVisible
                && <View style={{marginVertical: 5}}>
                    <Jobs style={{marginTop: 10}} pipelineId={_.get(pipeline, 'id')}/>
                </View>}

                <Stars repository={repository}/>
            </Card>
        );
    }
}

export const Stats = connect(state => ({
    repository: getRepository(state),
    branch: getDefaultBranchName(state),
    getPipeline: (projectId, branch) => getPipeline(state, projectId, branch)
}), {requestJobsForPipeline})(memo(StatsComponent));
