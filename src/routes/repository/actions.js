import React from 'react'; // eslint-disable-line no-unused-vars
import _ from 'lodash';

import {get, post} from 'api';
import {getGroupNameFromUrl, getRepoNameFromUrl} from 'utils';

import {getRepository} from './selectors';
import {getLiteExperienceValue} from '../settings/selectors';
import {requestMergeRequests} from '../mergeRequests/actions';
import {requestIssues} from '../issues/actions';
import {getDefaultBranchName} from '../branches/selectors';
import {requestBranches} from '../branches/actions';

export function clearFilelist() {
    return {type: 'CLEAR_FILELIST'};
}

export function clearReadme() {
    return {type: 'CLEAR_README'};
}

export function requestRepositoryApiCall(id, loader = true) {
    return get({path: `projects/${id}`, loader, additionalParams: {statistics: true}});
}

export function requestReadme() {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        if (!repo) {
            return null;
        }

        const readmeUrl = _.get(repo, 'readme_url');

        if (readmeUrl) {
            const urlParts = readmeUrl.split('/');
            const readmeFile = encodeURI(urlParts[urlParts.length - 1]);
            const ref = urlParts[urlParts.length - 2];
            const {data} = await get({
                path: `projects/${repo.id}/repository/files/${readmeFile}/raw`,
                additionalParams: {ref},
                pagination: false
            });

            if (data) {
                return dispatch({type: 'README_RECEIVED', data});
            }
        }
    };
}

export function requestRepository(id) {
    return async (dispatch, getState) => {
        const state = getState();

        const repoInTheState = getRepository(state);

        if (_.isEmpty(repoInTheState) && !id) {
            return;
        }

        const isLite = getLiteExperienceValue(state);
        const {data} = await requestRepositoryApiCall(id || repoInTheState.id);

        if (data) {
            dispatch({type: 'REPOSITORY_RECEIVED', data});
            dispatch(requestReadme());

            if (!isLite) {
                dispatch(requestMergeRequests());
                dispatch(requestBranches());
                dispatch(requestIssues());
            }
        }
    };
}

export function requestFileList(repo, subpath = '') {
    return async (dispatch, getState) => {
        const repository = repo || getRepository(getState());
        const branch = getDefaultBranchName(getState()) || '';

        if (_.isEmpty(repository)) {
            return null;
        }

        const {data} = await get({
            path: `projects/${repository.id}/repository/tree`,
            pagination: false,
            additionalParams: {
                path: subpath,
                ref: branch,
                per_page: 100
            }
        });

        if (data) {
            dispatch({type: 'FILELIST_RECEIVED', data});
        }

        return null;
    };
}

export function fetchSubPath(subPath) {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        if (_.isEmpty(repo)) {
            return;
        }

        dispatch(requestFileList(repo, subPath));
    };
}

export function fetchBlob(blobId, name) {
    return async (dispatch, getState) => {
        const {id} = getRepository(getState());

        if (!id) {
            return;
        }

        const {data} = await get({path: `projects/${id}/repository/blobs/${blobId}/raw`});

        if (data) {
            dispatch({type: 'BLOB_RECEIVED', name, data});
        }
    };
}

export function starProject() {
    return async (dispatch, getState) => {
        const {id} = getRepository(getState());

        if (!id) {
            return;
        }

        const starred = await post({path: `projects/${id}/star`, silent: true});

        if (!starred) {
            await post({path: `projects/${id}/unstar`, silent: true});
        }

        dispatch(requestRepository(id));
    };
}

export function forkProject(namespace) {
    return async (_dispatch, getState) => {
        const {id} = getRepository(getState());

        if (!id) {
            return;
        }

        await post({path: `projects/${id}/fork`, body: {namespace}});
    };
}

export async function searchRepoByUrl(url) {
    if (!url) {
        return null;
    }
    const repoGroup = getGroupNameFromUrl(url);
    const repoName = getRepoNameFromUrl(url);
    const repository = await get({path: `groups/${repoGroup}/search`, additionalParams: {scope: 'projects', search: repoName}, pagination: false});

    const data = _.get(repository, 'data');

    if (!data) {
        return null;
    }

    return _.find(data, repo => _.includes(repo.http_url_to_repo, `${repoGroup}/${repoName}.git`));
}

export function requestMembers() {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        let projectMembers = [];
        let groupMembers = [];

        if (!repo) {
            return null;
        }

        const hasGroup = _.get(repo, 'namespace.kind') === 'group';
        const projectM = await get({path: `projects/${repo.id}/members`, loader: false});

        if (projectM.data) {
            projectMembers = projectM.data;
        }

        if (hasGroup) {
            const groupId = _.get(repo, 'namespace.id');
            const groupM = await get({path: `groups/${groupId}/members`, loader: false});

            if (groupM.data) {
                groupMembers = groupM.data;
            }
        }

        const data = _.uniqBy([...projectMembers, ...groupMembers], 'id');

        if (data) {
            dispatch({type: 'MEMBERS_RECEIVED', data});
        }

        return null;
    };
}
