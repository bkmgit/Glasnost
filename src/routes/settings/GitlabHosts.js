import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {navigate} from 'router';
import {Card, colors, FormattedText, Icon, MainButton} from 'elements';
import {getGitlabUrls} from 'utils';

import {changeHost, removeHostPopup} from './actions';

function GitlabHostsComponent({gitlabUrls, changeHost, removeHostPopup, navigate}) {
    return (
        <Card title={'Gitlab Hosts'} icon={{name: 'tanuki'}}>
            <View style={{marginBottom: 30}}>
                {_.map(gitlabUrls, ({url}, index) => (
                    <View key={index} style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => changeHost(url)}>
                            <FormattedText style={{fontSize: 18}}>{url}</FormattedText>
                        </TouchableOpacity>
                        <Icon name={'merge-request-close-m'} color={colors.warning} size={25} onPress={() => removeHostPopup(url)}/>
                    </View>
                ))}
            </View>
            <MainButton text={'Add new Host'} onPress={() => navigate('AddGitlabUrl')}/>
        </Card>
    );
}

export const GitlabHosts = connect(state => ({gitlabUrls: getGitlabUrls(state)}), {removeHostPopup, changeHost, navigate})(GitlabHostsComponent);
