import React from 'react';
import {Switch} from 'react-native';
import {connect} from 'react-redux';

import {Card, FormattedText} from 'elements';
import {setData} from 'utils';

import {getLiteExperienceValue} from './selectors';
import {setLiteExperience} from './actions';

function LiteExperienceComponent({isLite, setLiteExperience}) {
    return (
        <Card style={{content: {flexDirection: 'row', justifyContent: 'space-between'}}} title={'Lite view'} icon={{name: 'template'}}>
            <FormattedText style={{fontSize: 18}}>Lite Experience:</FormattedText>
            <Switch value={isLite} onValueChange={() => {
                setData('liteExperience', String(!isLite));
                setLiteExperience(String(!isLite));
            }}/>
        </Card>
    );
}

export const LiteExperience = connect(state => ({isLite: getLiteExperienceValue(state)}), {setLiteExperience})(LiteExperienceComponent);
