import React from 'react';

import {registerRoute} from 'router';
import {AppContainer} from 'elements';
import {Fingerprint} from './Fingerprint';
import {DarkTheme} from './DarkTheme';
import {LiteExperience} from './LiteExperience';
import {GitlabHosts} from './GitlabHosts';

function Settings() {
    return (
        <AppContainer style={{padding: 20}}>
            <DarkTheme/>
            <LiteExperience/>
            <Fingerprint/>
            <GitlabHosts/>
        </AppContainer>
    );
}

registerRoute({Settings});
