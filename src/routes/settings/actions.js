import React from 'react';
import _ from 'lodash';

import {hidePopup, logout, setUrls, showPopup, parseLodash} from 'utils';
import {getData, setData} from 'utils/storage';
import {setCurrentUrl} from 'utils/actions';
import {loadConfig} from 'api';
import {navigate} from 'router';

import {MainButton} from 'elements/button';
import {requestMe} from '../profile/actions';
import {clearRepositories, requestRepositories} from '../repositories/actions';

export function setTheme(isDark) {
    return {type: 'SET_THEME', isDark};
}

export function setLiteExperience(isLite) {
    return {type: 'SET_LITE_EXPERIENCE', isLite};
}

export function setFingerprint(isSet) {
    return {type: 'SET_FINGERPRINT', isSet};
}

export function checkAndSetFingerprint(isSet) {
    return async (dispatch) => {
        dispatch(setFingerprint(String(isSet)));
        await setData('needsFingerprint', String(isSet));
    };
}

export function removeHost(host) {
    return async (dispatch) => {
        const dataFromStorage = await getData();
        let gitlabUrls = parseLodash(_.get(dataFromStorage, 'gitlabUrls', '[]'));
        const currentUrl = _.get(dataFromStorage, 'currentUrl', '');

        gitlabUrls = _.filter(gitlabUrls, ({url}) => url !== host);
        await setData('gitlabUrls', JSON.stringify(gitlabUrls));
        dispatch(setUrls(gitlabUrls));

        if (currentUrl === host) {
            return dispatch(logout(host));
        }

        return null;
    };
}

export function removeHostPopup(url) {
    return dispatch => dispatch(showPopup({
        content: `Do you really want to remove ${url} ?`,
        buttons: [
            <MainButton
                key={'Remove'}
                text={'Remove'}
                onPress={() => {
                    dispatch(removeHost(url));
                    dispatch(hidePopup());
                }}/>,
            <MainButton
                key={'Dismiss'}
                text={'Dismiss'}
                onPress={() => dispatch(hidePopup())}
                secondary={true}/>
        ]
    }));
}

export function changeHost(url) {
    return async (dispatch) => {
        const tokens = parseLodash(_.get(await getData(), 'userToken', '[]'));
        const singleToken = _.find(tokens, {url});

        if (singleToken) {
            const {accessToken, type} = singleToken;

            loadConfig(url, accessToken, type);
            dispatch(setCurrentUrl(url));
            dispatch(clearRepositories());

            setData('currentUrl', url);
            dispatch(requestMe());
            dispatch(navigate('Repositories'));
            await dispatch(requestRepositories());
        }
    };
}
