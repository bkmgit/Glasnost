import _ from 'lodash';
import {createSelector} from 'reselect';

const getSettings = state => _.get(state, 'settings');
export const getThemeValue = createSelector(getSettings, settings => _.get(settings, 'darkTheme', true));
export const getLiteExperienceValue = createSelector(getSettings, settings => _.get(settings, 'liteExperience', false));
export const getFingerprintValue = createSelector(getSettings, settings => _.get(settings, 'fingerprint', false));
