import React, {Component} from 'react';
import {FlatList, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {setFooter, setHeaderLeftButton, MainFooter} from 'headerFooter';
import {
    AppContainer,
    CircledImage,
    EmptyData,
    FormattedText,
    ItemList,
    LoadMore
} from 'elements';
import {navigate, registerRoute} from 'router';

import {getLastUpdatedTime, navigateFromUrl, setPagination} from 'utils';
import {getTodos} from './selectors';
import {clearTodos, requestTodos} from './actions';
import {MyFace} from '../repositories/MyFace';

class TodosComponent extends Component {
    componentDidMount() {
        const {requestTodos, setFooter, setHeaderLeftButton} = this.props;

        setHeaderLeftButton(this.headerLeftButton());
        setFooter(this.footer());
        requestTodos();
    }

    headerLeftButton() {
        return <MyFace/>;
    }

    footer() {
        return <MainFooter/>;
    }

    componentWillUnmount() {
        this.props.clearTodos();
        this.props.setPagination('todos', {'x-page': 0});
    }

    onScroll({nativeEvent}) {
        const {setFooter} = this.props;
        const currentOffset = nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);

        if (dif < 0) {
            setFooter(this.footer());
        } else {
            setFooter();
        }

        this.offset = currentOffset;
    }

    render() {
        const {todos, requestTodos, navigate, navigateFromUrl} = this.props;

        return (
            <AppContainer>
                {!_.size(todos) && <EmptyData/>}
                <FlatList
                    keyExtractor={item => `${item.id}`}
                    data={todos}
                    refreshing={false}
                    onRefresh={() => requestTodos(true)}
                    onScroll={e => this.onScroll(e)}
                    ListFooterComponent={<LoadMore entityName={'todos'} onPress={() => requestTodos(false)}/>}
                    renderItem={({item}) => <SingleTodo navigateFromUrl={navigateFromUrl} navigate={navigate} todo={item}/>}/>
            </AppContainer>
        );
    }
}

function Description({todo}) {
    const {target_url, target_type, author, action_name, created_at} = todo;
    const needOn = ['addressed', 'directly_addressed', 'mentioned'];
    const withoutYou = ['unmergeable'];
    const {unit, value} = getLastUpdatedTime(created_at);
    const cleanUrl = target_url.replace(/^.*\/\/[^\/]+\//, '').replace(/#note_\d+/, '');

    return (
        <View style={{marginHorizontal: 15}}>
            <View style={{flexDirection: 'row'}}>
                <FormattedText style={{fontWeight: 'bold'}}>{author.name}</FormattedText>
                <FormattedText>
                    {` ${action_name.replace('_', ' ')}`}
                    {` ${!_.includes(withoutYou, action_name) ? 'you' : ''}`}
                    {` ${_.includes(needOn, action_name) ? 'on' : ''}`}
                </FormattedText>
            </View>
            <FormattedText style={{fontWeight: 'bold'}}>{target_type}</FormattedText>
            <FormattedText style={{fontWeight: 'bold'}}>{cleanUrl}</FormattedText>
            <FormattedText style={{fontStyle: 'italic'}}>{`${value} ${unit} ago`}</FormattedText>
        </View>
    );
}

function SingleTodo({todo, navigate, navigateFromUrl}) {
    const {author, project: {id}, target_url} = todo;

    return (
        <ItemList onPress={() => navigateFromUrl({url: target_url, projectId: id, navigate})}>
            <CircledImage name={author.name} url={author.avatar_url} onPress={() => navigate('Profile', {id: author.id})}/>
            <Description todo={todo}/>
        </ItemList>
    );
}

const Todos = connect(state => ({todos: getTodos(state)}), {
    requestTodos, clearTodos, navigateFromUrl, navigate, setFooter, setHeaderLeftButton, setPagination
})(TodosComponent);
registerRoute({Todos});
