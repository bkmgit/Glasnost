import _ from 'lodash';

import {get} from 'api';
import {getPagination, setPagination} from 'utils';

export function requestTodos(page0) {
    return async (dispatch, getState) => {
        const {pageToLoad = 0} = getPagination(getState(), 'todos');

        const {data, headers} = await get({
            path: 'todos',
            page: page0 ? 0 : pageToLoad,
            cached: !page0
        });

        if (!_.isEmpty(data)) {
            dispatch({type: 'TODOS_RECEIVED', data});
            dispatch(setPagination('todos', headers));
        }
    };
}

export function clearTodos() {
    return dispatch => dispatch({type: 'CLEAR_TODOS'});
}
