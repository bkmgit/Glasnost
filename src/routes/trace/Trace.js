import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {ScrollableAppContainer, CodeDisplayer, AppContainer, colors, FormattedText} from 'elements';

import {setHeaderRightButton} from 'headerFooter';
import {Text, TouchableHighlight} from 'react-native';
import {registerRoute} from 'router';
import {getTrace} from './selectors';
import {requestTrace, clearTrace, playJob, cancelJob, retryJob} from './actions';

class TraceComponent extends Component {
    componentDidMount() {
        const {requestTrace, job} = this.props;

        requestTrace(job);
    }

    refresh() {
        const {job, clearTrace, requestTrace} = this.props;

        clearTrace(job);
        requestTrace(job);
    }

    componentWillUnmount() {
        this.props.clearTrace(this.props.job);
    }

    headerRightButton() {
        const {job, playJob, cancelJob, retryJob} = this.props;
        const {status} = job;

        if (status !== 'started' && status !== 'running' && status !== 'canceled' && status !== 'success') {
            return (
                <TouchableHighlight onPress={() => playJob(job)}>
                    <FormattedText>{'Start'}</FormattedText>
                </TouchableHighlight>
            );
        } if (status === 'running') {
            return (
                <TouchableHighlight onPress={() => cancelJob(job)}>
                    <Text style={{color: colors.warning}}>{'Stop'}</Text>
                </TouchableHighlight>
            );
        } if (status === 'canceled' || status === 'success') {
            return (
                <TouchableHighlight onPress={() => retryJob(job)}>
                    <FormattedText>{'Retry'}</FormattedText>
                </TouchableHighlight>
            );
        }

        return null;
    }

    render() {
        const {trace = '', setHeaderRightButton} = this.props;

        setHeaderRightButton(this.headerRightButton());

        return (
            <AppContainer>
                <ScrollableAppContainer scrollToBottom={true} style={{flex: 1}} refresh={() => this.refresh()}>
                    <CodeDisplayer language={'bash'}>{trace}</CodeDisplayer>
                </ScrollableAppContainer>
            </AppContainer>
        );
    }
}

const Trace = connect((state, {job}) => ({trace: getTrace(state, _.get(job, 'id'))}), {
    requestTrace, clearTrace, playJob, cancelJob, retryJob, setHeaderRightButton
})(TraceComponent);
registerRoute({Trace});
