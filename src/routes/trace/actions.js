import _ from 'lodash';
import {get, post} from 'api';

import {requestJobsForPipeline} from '../pipelines/actions';

export function requestTrace(job) {
    return async (dispatch) => {
        const {data} = await get({path: `projects/${job.projectId}/jobs/${job.id}/trace`, pagination: false});

        if (!_.isEmpty(data)) {
            dispatch({type: 'TRACE_RECEIVED', data, jobId: job.id});
        }
    };
}

export function playJob(job) {
    return (dispatch) => {
        post({path: `projects/${job.projectId}/jobs/${job.id}/play`});
        dispatch(requestJobsForPipeline(job.projectId, job.pipelineId, true));
    };
}

export function cancelJob(job) {
    return (dispatch) => {
        post({path: `projects/${job.projectId}/jobs/${job.id}/cancel`});
        dispatch(requestJobsForPipeline(job.projectId, job.pipelineId, true));
    };
}

export function retryJob(job) {
    return (dispatch) => {
        post({path: `projects/${job.projectId}/jobs/${job.id}/retry`});
        dispatch(requestJobsForPipeline(job.projectId, job.pipelineId, true));
    };
}

export function clearTrace(job) {
    return (dispatch) => {
        dispatch({type: 'TRACE_RECEIVED', trace: '', jobId: job.id});
    };
}
