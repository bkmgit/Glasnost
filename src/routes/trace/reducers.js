import {registerReducer} from 'store';


export function trace() {
    return {TRACE_RECEIVED: (state, {jobId, data}) => ({...state, [jobId]: data})};
}

registerReducer('trace', trace());
