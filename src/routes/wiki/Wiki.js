import {View} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {registerRoute} from 'router';
import {
    EmptyData,
    FormattedText, Markdown,
    ScrollableAppContainer
} from 'elements';

import {getGitlabUrl} from 'utils';
import {getWiki} from './selectors';
import {requestWiki} from './actions';
import {getRepository} from '../repository/selectors';

class WikiComponent extends Component {
    componentDidMount() {
        const {wikiSlug, requestWiki} = this.props;

        requestWiki(wikiSlug);
    }

    render() {
        const {wikiSlug, requestWiki, wiki, gitlabUrl, repo} = this.props;

        if (_.isEmpty(wiki)) {
            return <EmptyData/>;
        }

        const baseUrl = `${gitlabUrl}/${repo.path_with_namespace}`;

        return (
            <ScrollableAppContainer style={{flexDirection: 'column'}} refresh={() => requestWiki(wikiSlug)}>
                <View style={{marginBottom: 20}}>
                    <FormattedText style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}>{wiki.title}</FormattedText>
                    <FormattedText style={{textAlign: 'center', fontSize: 14, fontStyle: 'italic'}}>{wiki.slug}</FormattedText>
                </View>
                <Markdown baseUrl={baseUrl} text={wiki.content}/>
            </ScrollableAppContainer>
        );
    }
}

const Wiki = connect(state => ({
    wiki: getWiki(state),
    gitlabUrl: getGitlabUrl(state),
    repo: getRepository(state)
}), {requestWiki})(WikiComponent);
registerRoute({Wiki});
