import {FlatList, TextInput, View} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {setFooter, setHeaderLeftButton, setHeaderRightButton} from 'headerFooter';
import {navigate, getPreviousRouteName, registerRoute} from 'router';
import {
    Icon,
    mainInputStyle,
    ItemList,
    FormattedText,
    EmptyData, LoadMore, AppContainer
} from 'elements';

import {setPagination, shouldKeepRepoFooter} from 'utils';
import {getWikis} from './selectors';
import {clearWikis, requestWikis} from './actions';
import {Footer} from '../repository/Footer';

class WikisComponent extends Component {
    constructor() {
        super();

        this.state = {searchText: '', isSearchVisible: false, filteredData: []};
    }

    componentDidMount() {
        const {requestWikis, setHeaderRightButton, setHeaderLeftButton, setFooter} = this.props;

        requestWikis();
        setHeaderRightButton(this.headerRightButton());
        setHeaderLeftButton(this.headerLeftButton());
        setFooter(this.footer());
    }

    headerRightButton() {
        return <Icon name={'search'} size={30} onPress={() => this.toggleSearch()}/>;
    }

    headerLeftButton() {
        return <Icon name={'angle-left'} size={25} style={{padding: 5}} onPress={() => this.props.navigate('Repository')}/>;
    }

    footer() {
        const {previousRouteName} = this.props;

        return shouldKeepRepoFooter(previousRouteName) ? <Footer/> : null;
    }

    componentWillUnmount() {
        this.props.clearWikis();
        this.props.setPagination('wikis', {'x-page': 0});
    }

    toggleSearch() {
        const {isSearchVisible} = this.state;
        this.setState({isSearchVisible: !isSearchVisible});
    }

    filterData(searchText) {
        const {wikis} = this.props;

        if (!searchText) {
            return wikis;
        }

        const searchRegex = new RegExp(searchText, 'i');
        const filteredData = _.filter(wikis, wiki => searchRegex.test(wiki.title));

        return filteredData;
    }

    onScroll({nativeEvent}) {
        const {setFooter} = this.props;
        const currentOffset = nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);

        if (dif < 0) {
            setFooter(this.footer());
        } else {
            setFooter();
        }

        this.offset = currentOffset;
    }

    render() {
        const {requestWikis, navigate} = this.props;
        const {isSearchVisible, searchText} = this.state;

        const wikis = this.filterData(searchText);

        return (
            <AppContainer>
                {isSearchVisible && <TextInput
                    autoFocus={true}
                    placeholder={'Search'}
                    style={{...mainInputStyle, marginVertical: 10, marginHorizontal: 10}}
                    underlineColorAndroid={'transparent'}
                    value={searchText}
                    onChangeText={text => this.setState({searchText: text})}/>}
                {!_.size(wikis) && <EmptyData/>}
                <FlatList
                    ListFooterComponent={<LoadMore entityName={'wikis'} onPress={() => requestWikis(false)}/>}
                    keyExtractor={item => item.slug}
                    refreshing={false}
                    onRefresh={() => requestWikis(true)}
                    onScroll={e => this.onScroll(e)}
                    data={wikis}
                    renderItem={({item}) => <SingleWiki wiki={item} navigate={navigate}/>}/>
            </AppContainer>
        );
    }
}

function SingleWiki({wiki, navigate}) {
    const {title, slug} = wiki;

    return (
        <ItemList onPress={() => navigate('Wiki', {wikiSlug: encodeURIComponent(slug)})}>
            <View style={{flexDirection: 'column'}}>
                <FormattedText>{title}</FormattedText>
                <FormattedText style={{fontStyle: 'italic', fontSize: 12}}>{slug}</FormattedText>
            </View>
        </ItemList>
    );
}

const Wikis = connect(state => ({wikis: getWikis(state), previousRouteName: getPreviousRouteName(state)}), {
    requestWikis,
    clearWikis,
    setHeaderRightButton,
    setHeaderLeftButton,
    setFooter,
    navigate,
    setPagination
})(WikisComponent);
registerRoute({Wikis});
