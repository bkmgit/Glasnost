import {getRegisteredRoutes} from 'router';
import {getReducers} from 'store';

import * as index from '../index';

describe('Wikis index', () => {
    it('should export Wikis and reducers', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().Wikis).toBeDefined();
        expect(getReducers().mergeRequests).toBeDefined();
    });
});
